#pragma once

#include <memory>
namespace lab10
{
	template <typename T>
	class Node;

	template <typename T>
	class DoublyLinkedList
	{
	public:
		DoublyLinkedList();
		void Insert(std::unique_ptr<T> data);
		void Insert(std::unique_ptr<T> data, unsigned int index);
		bool Delete(const T& data);
		bool Search(const T& data) const;

		std::shared_ptr<Node<T>> operator[](unsigned int index) const;
		unsigned int GetLength() const;

	private:
		unsigned int mLength;
		std::shared_ptr<Node<T>> mHead;
		std::shared_ptr<Node<T>> mTail;
	};

	template <typename T>
	DoublyLinkedList<T>::DoublyLinkedList()
		: mLength(0)
		, mHead(nullptr)
		, mTail(nullptr)
	{

	}

	template <typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data)
	{
		if (mLength == 0)
		{
			mHead = std::make_shared<Node<T>>(std::move(data));
			mTail = mHead;
			mLength++;
			return;
		}
		mLength++;
		mTail->Next = std::make_shared<Node<T>>(std::move(data), mTail);
		mTail = mTail->Next;
	}

	template <typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data, unsigned int index)
	{
		if (mLength == 0 || mLength <= index)
		{
			Insert(std::move(data));
			return;
		}
		if (index == 0)
		{
			std::shared_ptr<Node<T>> temp2 = std::make_shared<Node<T>>(std::move(data));
			temp2->Next = mHead;
			mHead = temp2;
			temp2->Next->Previous = temp2;
			mLength++;
			return;
		}
		std::shared_ptr<Node<T>> temp = std::make_shared<Node<T>>(std::move(data), operator[](index - 1));
		temp->Next = operator[](index);
		temp->Next->Previous = temp;
		temp->Previous.lock()->Next = temp;
		mLength++;
	}

	template <typename T>
	bool DoublyLinkedList<T>::Delete(const T& data)
	{
		std::shared_ptr<Node<T>> temp = mHead;
		for (size_t i = 0; i < mLength; i++)
		{
			if (*temp->Data == data)
			{
				if (i == 0) // Head 일 경우
				{
					mHead = mHead->Next;
					mLength--;
					return true;
				}
				else if (i == mLength - 1) // Tail 일 경우
				{
					mTail = mTail->Previous.lock();
					mTail->Next = nullptr;
					mLength--;
					return true;
				}
				else // Head, Tail이 아닐 경우
				{
					temp->Next->Previous = temp->Previous.lock();
					temp->Previous.lock()->Next = temp->Next;
					mLength--;
					return true;
				}
			}
			temp = temp->Next;
		}
		return false;
	}

	template <typename T>
	bool DoublyLinkedList<T>::Search(const T& data) const
	{
		std::shared_ptr<Node<T>> temp = mHead;
		for (size_t i = 0; i < mLength; i++)
		{
			if (*temp->Data == data)
			{
				return true;
			}
			temp = temp->Next;
		}
		return false;
	}

	template <typename T>
	std::shared_ptr<Node<T>> DoublyLinkedList<T>::operator[](unsigned int index) const
	{
		if (index >= mLength)
		{
			return nullptr;
		}

		std::shared_ptr<Node<T>> temp = mHead;
		for (size_t i = 0; i < index; i++)
		{
			temp = temp->Next;
		}
		return temp;
	}

	template <typename T>
	unsigned int DoublyLinkedList<T>::GetLength() const
	{
		return mLength;
	}


}