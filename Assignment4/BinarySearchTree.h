#pragma once

#include <memory>
#include <vector>

namespace assignment4
{
	template <typename T>
	class TreeNode;

	template <typename T>
	class BinarySearchTree final
	{
	public:
		void Insert(std::unique_ptr<T> data);
		bool Search(const T& data);
		bool Delete(const T& data);
		const std::weak_ptr<TreeNode<T>> GetRootNode() const;

		static std::vector<T> TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode);
		static void TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode, std::vector<T>& v);
		const T GetMaxFromLeft(const std::shared_ptr<TreeNode<T>> startNode);
		const T GetMinFromRight(const std::shared_ptr<TreeNode<T>> startNode);
	private:
		std::shared_ptr<TreeNode<T>> mRootNode;
	};

	template <typename T>
	void BinarySearchTree<T>::Insert(std::unique_ptr<T> data)
	{
		if (mRootNode == nullptr)
		{
			mRootNode = std::make_shared<TreeNode<T>>(std::move(data));
			return;
		}
		
		std::shared_ptr<TreeNode<T>> node = mRootNode;
		while (true)
		{
			if (*(node->Data) >= *data)
			{
				if (node->Left == nullptr)
				{
					node->Left = std::make_shared<TreeNode<T>>(node, std::move(data));
					return;
				}
				node = node->Left;
			}
			else
			{
				if (node->Right == nullptr)
				{
					node->Right = std::make_shared<TreeNode<T>>(node, std::move(data));
					return;
				}
				node = node->Right;
			}
		}
	}

	template <typename T>
	const std::weak_ptr<TreeNode<T>> BinarySearchTree<T>::GetRootNode() const
	{
		return mRootNode;
	}

	template <typename T>
	bool BinarySearchTree<T>::Search(const T& data)
	{
		std::shared_ptr<TreeNode<T>> node = mRootNode;
		while (1)
		{
			if (node == nullptr)
			{
				return false;
			}
			if (*(node->Data) > data)
			{
				node = node->Left;
			}
			else if (*(node->Data) == data)
			{
				return true;
			}
			else
			{
				node = node->Right;
			}
		}
	}

	template <typename T>
	bool BinarySearchTree<T>::Delete(const T& data)
	{
		std::shared_ptr<TreeNode<T>> node = mRootNode;
		while (1)
		{
			if (node == nullptr)
			{
				return false;
			}
			if (*(node->Data) > data)
			{
				if (node->Left == nullptr)
				{
					return false;
				}
				if (*(node->Left->Data) == data)
				{
					// 단말 노드일 경우
					if (node->Left->Left == nullptr && node->Left->Right == nullptr)
					{
						node->Left.reset();
						return true;
					}
					// 자식 노드가 오른쪽 1개일 경우
					if (node->Left->Left == nullptr && node->Left->Right != nullptr)
					{
						node->Left = node->Left->Right;
						return true;
					}
					// 자식 노드가 왼쪽 1개일 경우
					if (node->Left->Left != nullptr && node->Left->Right == nullptr)
					{
						node->Left = node->Left->Left;
						return true;
					}
					// 자식 노드가 2개일 경우
					if (node->Left->Left != nullptr && node->Left->Right != nullptr)
					{
						T temp1 = GetMaxFromLeft(node->Left->Left);
						T temp2 = GetMinFromRight(node->Left->Right);
						// 왼쪽 최댓값이 더 가까울 경우.
						if (*node->Left->Data - temp1 <= temp2 - *node->Left->Data)
						{
							Delete(temp1);
							*(node->Left->Data) = temp1;
							return true;
						}
						else
						{
							Delete(temp2);
							*(node->Left->Data) = temp2;
							return true;
						}
					}
				}
				node = node->Left;
			}
			else if (*(node->Data) == data)
			{
				// 루트 혼자 있는 단말 노드일 경우.
				if (node->Left == nullptr && node->Right == nullptr)
				{
					node.reset();
					mRootNode.reset();
					return true;
				}
				// 자식 노드가 오른쪽 1개일 경우
				if (node->Left == nullptr && node->Right != nullptr)
				{
					mRootNode = mRootNode->Right;
					node.reset();
					return true;
				}
				// 자식 노드가 왼쪽 1개일 경우
				if (node->Left != nullptr && node->Right == nullptr)
				{
					mRootNode = mRootNode->Left;
					node.reset();
					return true;
				}
				// 자식 노드가 2개일 경우
				if (node->Left != nullptr && node->Right != nullptr)
				{
					T temp1 = GetMaxFromLeft(node->Left);
					T temp2 = GetMinFromRight(node->Right);
					// 왼쪽 최댓값이 더 가까울 경우.
					if (*node->Data - temp1 <= temp2 - *node->Data)
					{
						Delete(temp1);
						*(node->Data) = temp1;
						node.reset();
						return true;
					}
					else
					{
						Delete(temp2);
						*(node->Data) = temp2;
						node.reset();
						return true;
					}
				}
			}
			else
			{
				if (node->Right == nullptr)
				{
					return false;
				}
				if (*(node->Right->Data) == data)
				{
					// 단말 노드일 경우
					if (node->Right->Left == nullptr && node->Right->Right == nullptr)
					{
						node->Right.reset();
						return true;
					}
					// 자식 노드가 오른쪽 1개일 경우
					if (node->Right->Left == nullptr && node->Right->Right != nullptr)
					{
						node->Right = node->Right->Right;
						return true;
					}
					// 자식 노드가 왼쪽 1개일 경우
					if (node->Right->Left != nullptr && node->Right->Right == nullptr)
					{
						node->Right = node->Right->Left;
						return true;
					}
					// 자식 노드가 2개일 경우
					if (node->Right->Left != nullptr && node->Right->Right != nullptr)
					{
						T temp1 = GetMaxFromLeft(node->Right->Left);
						T temp2 = GetMinFromRight(node->Right->Right);
						// 왼쪽 최댓값이 더 가까울 경우.
						if (*node->Right->Data - temp1 <= temp2 - *node->Right->Data)
						{
							Delete(temp1);
							*(node->Right->Data) = temp1;
							return true;
						}
						else
						{
							Delete(temp2);
							*(node->Right->Data) = temp2;
							return true;
						}
					}
				}
				node = node->Right;
			}
		}
		return false;
	}

	template <typename T>
	std::vector<T> BinarySearchTree<T>::TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode)
	{
		std::vector<T> v;
		TraverseInOrder(startNode, v);
		return v;
	}

	template <typename T>
	void BinarySearchTree<T>::TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode, std::vector<T>& v)
	{
		if (startNode != nullptr)
		{
			TraverseInOrder(startNode->Left, v);
			v.push_back(*(startNode->Data));
			TraverseInOrder(startNode->Right, v);
		}
	}

	template <typename T>
	const T BinarySearchTree<T>::GetMaxFromLeft(const std::shared_ptr<TreeNode<T>> startNode)
	{
		std::shared_ptr<TreeNode<T>> node = startNode;
		while (1)
		{
			if (node->Right == nullptr)
			{
				return *(node->Data);
			}
			node = node->Right;
		}
	}

	template <typename T>
	const T BinarySearchTree<T>::GetMinFromRight(const std::shared_ptr<TreeNode<T>> startNode)
	{
		std::shared_ptr<TreeNode<T>> node = startNode;
		while (1)
		{
			if (node->Left == nullptr)
			{
				return *(node->Data);
			}
			node = node->Left;
		}
	}

}