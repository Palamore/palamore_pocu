#include "CircleLawn.h"

namespace lab5
{
	CircleLawn::CircleLawn(unsigned int radius)
		: mRadius(radius)
		, mPi(3.14)
	{

	}

	CircleLawn::~CircleLawn()
	{

	}

	unsigned int CircleLawn::GetArea() const
	{
		double rad = static_cast<double>(mRadius);

		double result = rad * rad * mPi;


		return static_cast<unsigned int>(round(result));
	}
}