#include "RectangleLawn.h"

namespace lab5
{
	RectangleLawn::RectangleLawn(unsigned int width, unsigned int height)
		: mWidth(width)
		, mHeight(height)
	{

	}

	RectangleLawn::~RectangleLawn()
	{

	}

	unsigned int RectangleLawn::GetArea() const
	{
		return mWidth * mHeight;
	}

	unsigned int RectangleLawn::GetMinimumFencesCount() const
	{
		return mWidth * 8 + mHeight * 8;
	}

	unsigned int RectangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		unsigned int onemeter = GetMinimumFencesCount() / 4;
		
		switch (fenceType)
		{
		case RED_CEDAR:
			return onemeter * 6;
			break;
		case SPRUCE:
			return onemeter * 7;
			break;
		default:
			break;
		}
		return 0;
	}

}