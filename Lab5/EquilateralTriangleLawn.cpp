#include "EquilateralTriangleLawn.h"


namespace lab5 
{
	EquilateralTriangleLawn::EquilateralTriangleLawn(unsigned int length)
		: mLength(length)
	{

	}

	EquilateralTriangleLawn::~EquilateralTriangleLawn()
	{

	}

	unsigned int EquilateralTriangleLawn::GetArea() const
	{
		double result = sqrt(3) / 4 * mLength * mLength;
		return static_cast<unsigned int>(round(result));
	}

	unsigned int EquilateralTriangleLawn::GetMinimumFencesCount() const
	{
		return mLength * 12;
	}

	unsigned int EquilateralTriangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		unsigned int onemeter = GetMinimumFencesCount() / 4;

		switch (fenceType)
		{
		case RED_CEDAR:
			return onemeter * 6;
			break;
		case SPRUCE:
			return onemeter * 7;
			break;
		default:
			break;
		}
		return 0;
	}

}


