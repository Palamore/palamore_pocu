#include "Lawn.h"


namespace lab5
{
	Lawn::Lawn()
	{

	}

	Lawn::~Lawn()
	{

	}

	unsigned int Lawn::GetGrassPrice(eGrassType grassType) const
	{
		switch (grassType)
		{
		case BERMUDA:
			return GetArea() * 8;
			break;
		case BAHIA:
			return GetArea() * 5;
			break;
		case BENTGRASS:
			return GetArea() * 3;
			break;
		case PERENNIAL_RYEGRASS:
			return static_cast<unsigned int>(round(static_cast<double>(GetArea()) * 2.5));
			break;
		case ST_AUGUSTINE:
			return static_cast<unsigned int>(round(static_cast<double>(GetArea()) * 4.5));
			break;
		default:
			break;
		}
		return 0;
	}

	unsigned int Lawn::GetMinimumSodRollsCount() const
	{
		double area = static_cast<double>(GetArea());

		unsigned int result = 0;

		while (area > 0.000001)
		{
			area -= 0.3;
			result++;
		}

		return result;
	}

}