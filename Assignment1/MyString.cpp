#include "MyString.h"

namespace assignment1
{
	MyString::MyString(const char* s)
	{

		if (s == nullptr)
		{
			mSize = 0;
			mStr = new char[1];
			mStr[0] = '\0';
			return;
		}

		int a = 0;
		while (s[a] != '\0')
		{
			a++;
		}
		mSize = a;


		mStr = new char[mSize + 1];
		for (int i = 0; i < mSize + 1; i++)
		{
			mStr[i] = s[i];
		}
	}

	MyString::MyString(const MyString& other)
		: mSize(other.mSize)
	{
		mStr = new char[other.mSize + 1];
		for (int i = 0; i < other.mSize + 1; i++)
		{
			mStr[i] = other.mStr[i];
		}
	}

	MyString::~MyString()
	{
		delete[] mStr;
	}

	unsigned int MyString::GetLength() const
	{
		return mSize;
	}

	const char* MyString::GetCString() const
	{
		return mStr;
	}

	void MyString::Append(const char* s)
	{
		if (s == "")
		{
			return;
		}

		int a = 0;
		while (s[a] != '\0')
		{
			a++;
		}
		int sSize = a;

		char* tempStr = new char[mSize + sSize + 1];
		
		for (int i = 0; i < mSize; i++) // mSize, 
		{
			tempStr[i] = mStr[i];
		}
		for (int i = mSize; i < mSize + sSize + 1; i++)
		{
			tempStr[i] = s[i - mSize];
		}

		delete[] mStr;
		mSize += sSize;
		mStr = new char[mSize + 1];
		for (int i = 0; i < mSize + 1; i++)
		{
			mStr[i] = tempStr[i];
		}
		delete[] tempStr;
	}

	MyString MyString::operator+(const MyString& other) const
	{
		MyString newOne(mStr);
		newOne.Append(other.mStr);

		return newOne;
	}


	int MyString::IndexOf(const char* s)
	{

		if (s == nullptr)
		{
			return -1;
		}

		if (s == "")
		{
			return 0;
		}

		int a = 0;
		while (s[a] != '\0')
		{
			a++;
		}
		int sSize = a;
		int temp = 0;
		for (int i = 0; i < mSize + 1; i++)
		{
			temp = 0;
			while (true)
			{
				if (mStr[i + temp] == s[temp])
				{
					temp++;
				}
				else
				{
					break;
				}
				if (temp == a)
				{
					return i;
				}
			}
		}
		return -1;
	}

	int MyString::LastIndexOf(const char* s)
	{
		if (s == nullptr)
		{
			return -1;
		}


		if (s == "")
		{
			return mSize;
		}

		int lastOne = -1;
		int a = 0;
		while (s[a] != '\0')
		{
			a++;
		}
		int sSize = a;
		int temp = 0;
		for (int i = 0; i < mSize + 1; i++)
		{
			temp = 0;
			while (true)
			{
				if (mStr[i + temp] == s[temp])
				{
					temp++;
				}
				else
				{
					break;
				}
				if (temp == a)
				{
					lastOne = i;
				}
			}
		}

		return lastOne;
	}

	void MyString::Interleave(const char* s)
	{
		int a = 0; 
		while (s[a] != '\0')
		{
			a++;
		}

		int sSize = a;
		if (sSize == 0)
		{
			return;
		}

		int mCounter = 0, sCounter = 0;
		char* newOne = new char[mSize + sSize + 1];
		for (int i = 0; i < mSize + sSize + 1; i++)
		{
			if (i % 2 == 0)
			{
				if (mCounter != mSize)
				{
					newOne[i] = mStr[mCounter];
					mCounter++;
				}
				else
				{
					newOne[i] = s[sCounter];
					sCounter++;
				}
			}
			else
			{
				if (sCounter != sSize)
				{
					newOne[i] = s[sCounter];
					sCounter++;
				}
				else
				{
					newOne[i] = mStr[mCounter];
					mCounter++;
				}
			}
		}
		
		mSize += sSize;
		delete[] mStr;

		mStr = new char[mSize + 1];

		for (int i = 0; i < mSize + 1; i++)
		{
			mStr[i] = newOne[i];
		}
		delete[] newOne;

	}

	bool MyString::RemoveAt(unsigned int index)
	{
		
		if (mSize <= static_cast<int>(index))
		{
			return false;
		}
		char* newOne = new char[mSize];
		int tempIndex = 0;
		for (int i = 0; i < mSize + 1; i++)
		{
			if (i != static_cast<int>(index))
			{
				newOne[tempIndex] = mStr[i];
				tempIndex++;
			}
		}
		mSize--;
		delete[] mStr;
		mStr = new char[mSize + 1];
		for (int i = 0; i < mSize + 1; i++)
		{
			mStr[i] = newOne[i];
		}
		delete[] newOne;

		return true;
	}

	void MyString::PadLeft(unsigned int totalLength)
	{
		if (static_cast<int>(totalLength) <= mSize)
		{
			return;
		}
		int interval = static_cast<int>(totalLength) - mSize;

		MyString newOne(" ");

		for (int i = 0; i < interval - 1; i++)
		{
			newOne.Append(" ");
		}
		newOne.Append(mStr);

		mSize = static_cast<int>(totalLength);

		delete[] mStr;

		mStr = new char[mSize + 1];

		for (int i = 0; i < mSize + 1; i++)
		{
			mStr[i] = newOne.mStr[i];
		}
	}

	void MyString::PadLeft(unsigned int totalLength, const char c)
	{
		if (static_cast<int>(totalLength) <= mSize)
		{
			return;
		}
		int interval = static_cast<int>(totalLength) - mSize;

		
		MyString newOne(&c);

		for (int i = 0; i < interval - 1; i++)
		{
			newOne.Append(&c);
		}
		newOne.Append(mStr);

		mSize = static_cast<int>(totalLength);

		delete[] mStr;

		mStr = new char[mSize + 1];

		for (int i = 0; i < mSize + 1; i++)
		{
			mStr[i] = newOne.mStr[i];
		}
	}

	void MyString::PadRight(unsigned int totalLength)
	{
		if (static_cast<int>(totalLength) <= mSize)
		{
			return;
		}
		int interval = static_cast<int>(totalLength) - mSize;

		for (int i = 0; i < interval; i++)
		{
			this->Append(" ");
		}
	}

	void MyString::PadRight(unsigned int totalLength, const char c)
	{
		if (static_cast<int>(totalLength) <= mSize)
		{
			return;
		}
		int interval = static_cast<int>(totalLength) - mSize;

		for (int i = 0; i < interval; i++)
		{
			this->Append(&c);
		}
	}

	void MyString::Reverse()
	{
		if (mSize == 0)
		{
			return;
		}

		unsigned int pivotA = 0;
		unsigned int pivotB = mSize - 1;
		char temp = 0;
		while (pivotA < pivotB)
		{
			temp = mStr[pivotA];
			mStr[pivotA] = mStr[pivotB];
			mStr[pivotB] = temp;
			pivotA++;
			pivotB--;
		}

	}

	bool MyString::operator==(const MyString& rhs) const
	{
		if (mSize != rhs.mSize)
		{
			return false;
		}



		for (int i = 0; i < mSize; i++)
		{
			if (mStr[i] != rhs.mStr[i])
			{
				return false;
			}
		}
		return true;
		
	}

	MyString& MyString::operator=(const MyString& rhs)
	{
		mSize = rhs.mSize;

		char* tempStr = new char[rhs.mSize + 1]; // Heap Allocate

		for (int i = 0; i < rhs.mSize + 1; i++)
		{
			tempStr[i] = rhs.mStr[i];
		}

		

		delete[] mStr;

		

		mStr = new char[rhs.mSize + 1];

		for (int i = 0; i < rhs.mSize + 1; i++)
		{
			mStr[i] = tempStr[i];
		}
		
		delete[] tempStr;                           //Heap Deallocate


		return *this;
	}

	void MyString::ToLower()
	{
		for (int i = 0; i < mSize; i++)
		{
			if (mStr[i] >= 65 && mStr[i] <= 90)
			{
				mStr[i] += 32;
			}
		}

	}

	void MyString::ToUpper()
	{
		for (int i = 0; i < mSize; i++)
		{
			if (mStr[i] >= 97 && mStr[i] <= 122)
			{
				mStr[i] -= 32;
			}
		}
	}

}