#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

namespace lab7
{
	template <typename T>
	std::vector<T> ConvertVectorToUniqueVector(const std::vector<T>& v)
	{
		std::vector<T> uVector;
		uVector.reserve(v.size());
		typename std::vector<T>::iterator iter;
		for (size_t i = 0; i < v.size(); i++)
		{
			iter = find(uVector.begin(), uVector.end(), v[i]);
			if (iter == uVector.end())
			{
				uVector.push_back(v[i]);
			}
		}
		return uVector;
	}
	
	template <typename K, class V>
	std::map<K, V> ConvertMapToUniqueMap(const std::map<K, V>& m)
	{
		std::map<K, V> uMap;
		typename std::map<K, V>::const_iterator mIter = m.begin();
		for (size_t i = 0; i < m.size(); i++)
		{
			if (uMap.find(mIter->first) == uMap.end())
			{
				uMap.emplace(mIter->first, mIter->second);
			}
			mIter++;
		}
		return uMap;
	}
	

	template <typename K, class V>
	std::map<K, V> ConvertVectorsToMap(const std::vector<K>& keys, const std::vector<V>& values)
	{
		std::vector<K> uniqueKeys = ConvertVectorToUniqueVector(keys);
		std::map<K, V> m;
		unsigned int minSize = uniqueKeys.size() < values.size() ? uniqueKeys.size() : values.size();
		for (size_t i = 0; i < minSize; i++)
		{
			m.emplace(uniqueKeys[i], values[i]);
		}
		return m;
	}

	template <typename K, class V>
	std::vector<K> GetKeys(const std::map<K, V>& m)
	{
		std::vector<K> v;
		v.reserve(m.size());
		typename std::map<K, V>::const_iterator iter = m.begin();
		for (size_t i = 0; i < m.size(); i++)
		{
			v.push_back(iter->first);
			iter++;
		}
		return v;
	}

	template <typename K, class V>
	std::vector<V> GetValues(const std::map<K, V>& m)
	{
		std::vector<V> v;
		v.reserve(m.size());
		typename std::map<K, V>::const_iterator iter = m.begin();
		for (size_t i = 0; i < m.size(); i++)
		{
			v.push_back(iter->second);
			iter++;
		}
		return v;
	}

	template <typename T>
	std::vector<T> Reverse(const std::vector<T>& v)
	{
		std::vector<T> rv = v;
		typename std::vector<T>::iterator beginIter = rv.begin();
		typename std::vector<T>::iterator endIter = rv.end();
		endIter--;
		T temp;
		while (beginIter < endIter)
		{
			temp = *beginIter;
			*beginIter = *endIter;
			*endIter = temp;

			beginIter++;
			endIter--;
		}
		return rv;
	}

	template <typename T>
	std::vector<T> operator+(const std::vector<T>& v1, const std::vector<T>& v2)
	{
		std::vector<T> combined;
		combined.reserve(v1.size() + v2.size());
		for (size_t i = 0; i < v1.size(); i++)
		{
			combined.push_back(v1[i]);
		}
		for (size_t i = 0; i < v2.size(); i++)
		{
			combined.push_back(v2[i]);
		}
		combined = ConvertVectorToUniqueVector(combined);

		return combined;
	}

	template <typename K, class V>
	std::map<K, V> operator+(const std::map<K, V>& m1, const std::map<K, V>& m2)
	{
		std::map<K, V> combined;
		typename std::map<K, V>::const_iterator m1Iter = m1.begin();
		typename std::map<K, V>::const_iterator m2Iter = m2.begin();

		for (size_t i = 0; i < m1.size(); i++)
		{
			combined.emplace(m1Iter->first, m1Iter->second);
			m1Iter++;
		}
		for (size_t i = 0; i < m2.size(); i++)
		{
			combined.emplace(m2Iter->first, m2Iter->second);
			m2Iter++;
		}
		combined = ConvertMapToUniqueMap(combined);
		return combined;
	}

	template <typename T>
	std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
	{
		for (size_t i = 0; i < v.size(); i++)
		{
			os << v[i];
			if (i != v.size() - 1)
			{
				os << ", ";
			}
		}
		return os;
	}

	template <typename K, class V>
	std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m)
	{
		typename std::map<K, V>::const_iterator iter = m.begin();
		for (size_t i = 0; i < m.size(); i++)
		{
			os << "{ " << iter->first << ", " << iter->second << " }\n";
			iter++;
		}
		return os;
	}

}