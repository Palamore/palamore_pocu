#pragma once

#include "Person.h"

namespace assignment2
{
	class Vehicle
	{
	public:
		Vehicle(unsigned int maxPassengersCount);
		virtual ~Vehicle();

		Vehicle(const Vehicle& other);
		Vehicle& operator=(const Vehicle& other);

		virtual unsigned int GetMaxSpeed() const = 0;
		virtual void ExecuteTravel(unsigned int index) const = 0;
		
		bool AddPassenger(const Person* person);
		bool RemovePassenger(unsigned int i);
		const Person* GetPassenger(unsigned int i) const;
		unsigned int GetPassengersCount() const;
		unsigned int GetMaxPassengersCount() const;
	
	protected:
		unsigned int getPassengersWeight() const;
		const Person** mPassengers;
		double mEulerNumb;
		unsigned int mMaxPassengersCount;
		unsigned int mPassengersCount;

	private:



	};
}