#pragma once

#include "Vehicle.h"
#include "IDrivable.h"
#include "DeusExMachina.h"

namespace assignment2
{
	class Motorcycle : public Vehicle, IDrivable 
	{
	public:
		Motorcycle();
		~Motorcycle();
		virtual unsigned int GetMaxSpeed() const;
		virtual unsigned int GetDriveSpeed() const;
		virtual void ExecuteTravel(unsigned int index) const;

	private:
		bool mbTravelPlan[6];
	};
}