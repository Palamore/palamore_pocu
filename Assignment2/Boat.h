#pragma once

#include "Boatplane.h"
#include "ISailable.h"
#include "Airplane.h"

#include "DeusExMachina.h"

namespace assignment2
{
	class Airplane;

	class Boat : public Vehicle, ISailable
	{
	public:
		Boat(unsigned int maxPassengersCount);
		~Boat();

		Boatplane operator+(Airplane& plane);


		virtual unsigned int GetMaxSpeed() const;
		virtual unsigned int GetSailSpeed() const;
		virtual void ExecuteTravel(unsigned int index) const;
		void TransferToBoatplane();
	private:
		bool mbTravelPlan[3];
	};
}