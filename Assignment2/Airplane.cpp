#include "Airplane.h"
#include "Boat.h"
#include "Boatplane.h"

namespace assignment2
{
	Airplane::Airplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		mbTravelPlan[0] = true;
		mbTravelPlan[1] = false;
		mbTravelPlan[2] = false;
		mbTravelPlan[3] = false;
	}

	Airplane::~Airplane()
	{

	}

	Boatplane Airplane::operator+(Boat& boat)
	{
		unsigned int fullLength = GetMaxPassengersCount() + boat.GetMaxPassengersCount();
		const Person** tmpPassengers = new const Person * [fullLength];
		unsigned int actualLength = GetPassengersCount() + boat.GetPassengersCount();

		for (size_t i = 0; i < GetPassengersCount(); i++)
		{
			tmpPassengers[i] = GetPassenger(i);
		}
		for (size_t i = GetPassengersCount(); i < actualLength; i++)
		{
			tmpPassengers[i] = boat.GetPassenger(i - GetPassengersCount());
		}
		TransferToBoatplane();
		boat.TransferToBoatplane();

		return Boatplane(fullLength, actualLength, tmpPassengers);
	}

	unsigned int Airplane::GetMaxSpeed() const
	{
		if (GetFlySpeed() > GetDriveSpeed())
		{
			return GetFlySpeed();
		}
		return GetDriveSpeed();
	}

	unsigned int Airplane::GetFlySpeed() const
	{
		double result = 0.0;
		result = 200 * (pow(mEulerNumb, (800.0 - static_cast<double>(getPassengersWeight())) / 500.0));

		unsigned int flySpeed = static_cast<unsigned int>(round(result));

		return flySpeed;
	}

	unsigned int Airplane::GetDriveSpeed() const
	{
		double result = 0.0;
		result = 4 * (pow(mEulerNumb, (400.0 - static_cast<double>(getPassengersWeight())) / 70.0));

		unsigned int driveSpeed = static_cast<unsigned int>(round(result));

		return driveSpeed;
	}

	void Airplane::TransferToBoatplane()
	{
		for (size_t i = 0; i < mPassengersCount; i++)
		{
			mPassengers[i] = nullptr;
		}
		mPassengersCount = 0;
	}

	void Airplane::ExecuteTravel(unsigned int index) const
	{
		unsigned int travelCount = DeusExMachina::GetInstance()->TravelCount[index];
		while (travelCount >= 4)
		{
			travelCount -= 4;
		}
		if (mbTravelPlan[travelCount])
		{
			DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
		}
	}



}