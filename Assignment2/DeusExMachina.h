#pragma once

#include "Vehicle.h"

namespace assignment2
{
	class DeusExMachina
	{
	public:

		~DeusExMachina();
		DeusExMachina& operator=(const DeusExMachina& rhs);
		static DeusExMachina* GetInstance();
		void Travel() const;
		bool AddVehicle(Vehicle* vehicle);
		bool RemoveVehicle(unsigned int i);
		const Vehicle* GetFurthestTravelled() const;
		Vehicle* GetVehicle(unsigned int index) const;
		unsigned int* TravelledDistanceHolder;
		unsigned int* TravelCount;
	private:
		DeusExMachina();
		static DeusExMachina* mInstance;
		Vehicle** mVehicles;
		unsigned int mVehiclesCount;
	};
}