#include "UBoat.h"

namespace assignment2
{
	UBoat::UBoat()
		: Vehicle(50)
	{
		mbTravelPlan[0] = true;
		mbTravelPlan[1] = true;
		for (int i = 2; i < 6; i++)
		{
			mbTravelPlan[i] = false;
		}
	}
	UBoat::~UBoat()
	{

	}

	unsigned int UBoat::GetMaxSpeed() const
	{
		if (GetDiveSpeed() > GetSailSpeed())
		{
			return GetDiveSpeed();
		}
		return GetSailSpeed();
	}

	unsigned int UBoat::GetDiveSpeed() const
	{
		double result = 500.0 * log(static_cast<double>((getPassengersWeight()) + 150.0) / 150.0) + 30.0;
		return static_cast<unsigned int>(round(result));
	}

	unsigned int UBoat::GetSailSpeed() const
	{
		double result = 550.0 - (static_cast<double>(getPassengersWeight()) / 10.0);
		if (result <= 200.0)
		{
			return 200;
		}
		return static_cast<unsigned int>(round(result));
	}

	void UBoat::ExecuteTravel(unsigned int index) const
	{
		unsigned int travelCount = DeusExMachina::GetInstance()->TravelCount[index];
		while (travelCount >= 6)
		{
			travelCount -= 6;
		}
		if (mbTravelPlan[travelCount])
		{
			DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
		}
	}

}