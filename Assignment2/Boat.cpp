#include "Boat.h"

namespace assignment2
{
	Boat::Boat(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		mbTravelPlan[0] = true;
		mbTravelPlan[1] = true;
		mbTravelPlan[2] = false;
	}

	Boat::~Boat()
	{

	}

	Boatplane Boat::operator+(Airplane& plane)
	{
		unsigned int fullLength = GetMaxPassengersCount() + plane.GetMaxPassengersCount();
		const Person** tmpPassengers = new const Person * [fullLength];
		unsigned int actualLength = GetPassengersCount() + plane.GetPassengersCount();

		for (size_t i = 0; i < plane.GetPassengersCount(); i++)
		{
			tmpPassengers[i] = plane.GetPassenger(i);
		}
		for (size_t i = plane.GetPassengersCount(); i < actualLength; i++)
		{
			tmpPassengers[i] = GetPassenger(i - plane.GetPassengersCount());
		}

		plane.TransferToBoatplane();
		TransferToBoatplane();

		return Boatplane(fullLength, actualLength, tmpPassengers);
	}

	unsigned int Boat::GetMaxSpeed() const
	{
		return GetSailSpeed();
	}

	unsigned int Boat::GetSailSpeed() const
	{
		
		unsigned int result = 800 - (10 * getPassengersWeight());
		if (result < 20 || getPassengersWeight() > 80)
		{
			return 20;
		}
		return result;
	}

	void Boat::TransferToBoatplane()
	{
		for (size_t i = 0; i < mPassengersCount; i++)
		{
			mPassengers[i] = nullptr;
		}
		mPassengersCount = 0;
	}


	void Boat::ExecuteTravel(unsigned int index) const
	{
		unsigned int travelCount = DeusExMachina::GetInstance()->TravelCount[index];
		while (travelCount >= 3)
		{
			travelCount -= 3;
		}
		if (mbTravelPlan[travelCount])
		{
			DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
		}
	}


}