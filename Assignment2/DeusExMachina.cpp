#include "DeusExMachina.h"

namespace assignment2
{
	DeusExMachina* DeusExMachina::mInstance = new DeusExMachina();
	DeusExMachina::DeusExMachina()
		: mVehiclesCount(0)
	{
		mVehicles = new Vehicle * [10];
		TravelledDistanceHolder = new unsigned int[10];
		TravelCount = new unsigned int[10];
		for (int i = 0; i < 10; i++)
		{
			TravelledDistanceHolder[i] = 0;
			TravelCount[i] = 0;
		}
	}

	DeusExMachina::~DeusExMachina()
	{

		for (unsigned int i = 0; i < mVehiclesCount; i++)
		{
			delete mVehicles[i];
		}
		delete[] TravelCount;
		delete[] TravelledDistanceHolder;
		delete[] mVehicles;
	}

	DeusExMachina& DeusExMachina::operator=(const DeusExMachina& rhs)
	{
		Vehicle** tempVehicles = new Vehicle * [10];
		for (unsigned int i = 0; i < rhs.mVehiclesCount; i++)
		{
			tempVehicles[i] = rhs.mVehicles[i];
		}

		delete[] mVehicles;

		mVehicles = new Vehicle * [10];

		for (size_t i = 0; i < rhs.mVehiclesCount; i++)
		{
			mVehicles[i] = tempVehicles[i];
		}
		mVehiclesCount = rhs.mVehiclesCount;
		delete[] tempVehicles;

		for (int i = 0; i < 10; i++)
		{
			TravelledDistanceHolder[i] = rhs.TravelledDistanceHolder[i];
			TravelCount[i] = rhs.TravelCount[i];
		}
		

		return *this;
	}

	DeusExMachina* DeusExMachina::GetInstance()
	{
		if (mInstance == nullptr)
		{
			mInstance = new DeusExMachina();
		}
		return mInstance;
	}

	void DeusExMachina::Travel() const
	{
		for (unsigned int i = 0; i < mVehiclesCount; i++)
		{
			mVehicles[i]->ExecuteTravel(i);
			DeusExMachina::GetInstance()->TravelCount[i]++;
		}
	}

	bool DeusExMachina::AddVehicle(Vehicle* vehicle)
	{
		
		if (vehicle == NULL)
		{
			return false;
		}
		
		if (mVehiclesCount == 10)
		{
			return false;
		}
		DeusExMachina::GetInstance()->TravelledDistanceHolder[mVehiclesCount] = 0;
		DeusExMachina::GetInstance()->TravelCount[mVehiclesCount] = 0;
		mVehicles[mVehiclesCount] = vehicle;
		mVehiclesCount++;
		return true;
	}

	bool DeusExMachina::RemoveVehicle(unsigned int i)
	{
		if (i >= mVehiclesCount || i < 0)
		{
			return false;
		}

		delete mVehicles[i];
		for (unsigned int j = i; j < mVehiclesCount - 1; j++)
		{
			mVehicles[j] = mVehicles[j + 1];
			DeusExMachina::GetInstance()->TravelledDistanceHolder[j] = DeusExMachina::GetInstance()->TravelledDistanceHolder[j + 1];
			DeusExMachina::GetInstance()->TravelCount[j] = DeusExMachina::GetInstance()->TravelCount[j + 1];
		}
		DeusExMachina::GetInstance()->TravelledDistanceHolder[mVehiclesCount - 1] = 0;
		DeusExMachina::GetInstance()->TravelCount[mVehiclesCount - 1] = 0;
		mVehicles[mVehiclesCount - 1] = nullptr;
		mVehiclesCount--;
		return true;
	}

	const Vehicle* DeusExMachina::GetFurthestTravelled() const
	{
		if (mVehiclesCount == 0)
		{
			return NULL;
		}

		size_t tmpCounter = 0;
		while (DeusExMachina::GetInstance()->TravelCount[tmpCounter] == 0)
		{
			tmpCounter++;
			if (tmpCounter == mVehiclesCount)
			{
				return mVehicles[0];
			}
		}

		unsigned int oneFurthest = TravelledDistanceHolder[0];
		unsigned int indexer = 0;
		for (unsigned int i = 1; i < mVehiclesCount; i++)
		{
			if (oneFurthest < TravelledDistanceHolder[i])
			{
				oneFurthest = TravelledDistanceHolder[i];
				indexer = i;
			}
		}
		return mVehicles[indexer];
	}

	Vehicle* DeusExMachina::GetVehicle(unsigned int index) const
	{
		return mVehicles[index];
	}

}