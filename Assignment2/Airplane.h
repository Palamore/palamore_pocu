#pragma once

#include "Boatplane.h"
#include "Vehicle.h"
#include "IDrivable.h"
#include "IFlyable.h"
#include "Boat.h"
#include "DeusExMachina.h"

namespace assignment2
{
	class Boat;

	class Airplane : public Vehicle, IDrivable, IFlyable
	{
	public:
		Airplane(unsigned int maxPassengersCount);
		~Airplane();

		Boatplane operator+(Boat& boat);

		virtual unsigned int GetMaxSpeed() const;
		virtual unsigned int GetFlySpeed() const;
		virtual unsigned int GetDriveSpeed() const;
		virtual void ExecuteTravel(unsigned int index) const;
		void TransferToBoatplane();

	private:
		bool mbTravelPlan[4];
	};
}