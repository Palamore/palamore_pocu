#include "Sedan.h"

namespace assignment2
{
	Sedan::Sedan()
		: Vehicle(4)
	{
		mTrailer = nullptr;
		for (int i = 0; i < 5; i++)
		{
			mbTravelPlan[i] = true;
			mbTravelPlanTrailer[i] = true;
		}
		mbTravelPlan[5] = false;
		mbTravelPlanTrailer[5] = false;
		mbTravelPlanTrailer[6] = false;
	}

	Sedan::~Sedan()
	{
		if (mTrailer != nullptr)
		{
			delete mTrailer;
		}
	}

	Sedan::Sedan(const Sedan& rhs)
		: Vehicle(rhs)
	{
		
		if (rhs.mTrailer == nullptr)
		{
			mTrailer = nullptr;
		}
		else
		{
			mTrailer = new Trailer(rhs.mTrailer->GetWeight());
		}
		for (int i = 0; i < 5; i++)
		{
			mbTravelPlan[i] = true;
			mbTravelPlanTrailer[i] = true;
		}
		mbTravelPlan[5] = false;
		mbTravelPlanTrailer[5] = false;
		mbTravelPlanTrailer[6] = false;
	}
	
	Sedan& Sedan::operator=(const Sedan& other)
	{
		Vehicle* vP = static_cast<Vehicle*>(this);

		if (vP->GetPassenger(0) == other.GetPassenger(0))
		{
			return *this;
		}
		
		const Person** tmpPassengers = new const Person * [other.GetMaxPassengersCount()];
		for (size_t i = 0; i < other.GetPassengersCount(); i++)
		{
			tmpPassengers[i] = new Person(other.mPassengers[i]->GetName(), other.mPassengers[i]->GetWeight());
		}

		for (size_t i = 0; i < mPassengersCount; i++)
		{
			delete mPassengers[i];
		}

		delete[] mPassengers;

		mPassengers = tmpPassengers;

		mMaxPassengersCount = other.GetMaxPassengersCount();
		mPassengersCount = other.GetPassengersCount();
		mEulerNumb = other.mEulerNumb;


		delete mTrailer;

		if (other.mTrailer == nullptr)
		{
			mTrailer = nullptr;
		}
		else
		{
			mTrailer = new Trailer(other.mTrailer->GetWeight());
		}
		for (int i = 0; i < 5; i++)
		{
			mbTravelPlan[i] = true;
			mbTravelPlanTrailer[i] = true;
		}
		mbTravelPlan[5] = false;
		mbTravelPlanTrailer[5] = false;
		mbTravelPlanTrailer[6] = false;

		return *this;
	}
	

	bool Sedan::AddTrailer(const Trailer* trailer)
	{
		if (mTrailer != nullptr || trailer == NULL)
		{
			return false;
		}
		mTrailer = trailer;
		return true;
	}

	bool Sedan::RemoveTrailer()
	{
		if (mTrailer == nullptr)
		{
			return false;
		}
		delete mTrailer;
		mTrailer = nullptr;
		return true;
	}

	unsigned int Sedan::GetMaxSpeed() const
	{
		return GetDriveSpeed();
	}

	unsigned int Sedan::GetDriveSpeed() const
	{
		unsigned int totalWeigths = 0;
		totalWeigths += getPassengersWeight();
		if (mTrailer != nullptr)
		{
			totalWeigths += mTrailer->GetWeight();
		}
		if (totalWeigths <= 80)
		{
			return 480;
		}
		else if (totalWeigths > 80 && totalWeigths <= 160)
		{
			return 458;
		}
		else if (totalWeigths > 160 && totalWeigths <= 260)
		{
			return 400;
		}
		else if (totalWeigths > 260 && totalWeigths <= 350)
		{
			return 380;
		}
		else if (totalWeigths > 350)
		{
			return 300;
		}
		return 0;
	}

	void Sedan::ExecuteTravel(unsigned int index) const
	{
		unsigned int travelCount = DeusExMachina::GetInstance()->TravelCount[index];
		if (mTrailer == nullptr)
		{
			while (travelCount >= 6)
			{
				travelCount -= 6;
			}
			if (mbTravelPlan[travelCount])
			{
				DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
			}
			return;
		}
		while (travelCount >= 7)
		{
			travelCount -= 7;
		}
		if (mbTravelPlanTrailer[travelCount])
		{
			DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
		}
	}

}