#include "Motorcycle.h"

namespace assignment2
{
	Motorcycle::Motorcycle()
		: Vehicle(2)
	{
		for (int i = 0; i < 5; i++)
		{
			mbTravelPlan[i] = true;
		}
		mbTravelPlan[5] = false;
	}
	Motorcycle::~Motorcycle()
	{

	}

	unsigned int Motorcycle::GetMaxSpeed() const
	{
		return GetDriveSpeed();
	}

	unsigned int Motorcycle::GetDriveSpeed() const
	{
		double pWeights = static_cast<double>(getPassengersWeight());
		double result = (2.0 * pWeights) + 400 - pow(pWeights / 15.0, 3.0);
		if (result < 0)
		{
			return 0;
		}
		return static_cast<unsigned int>(round(result));
	}

	void Motorcycle::ExecuteTravel(unsigned int index) const
	{
		unsigned int travelCount = DeusExMachina::GetInstance()->TravelCount[index];
		while (travelCount >= 6)
		{
			travelCount -= 6;
		}
		if (mbTravelPlan[travelCount])
		{
			DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
		}
	}

}