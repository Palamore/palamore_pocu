#pragma once

#include "Vehicle.h"
#include "IDrivable.h"
#include "Trailer.h"
#include "DeusExMachina.h"

namespace assignment2
{
	class Trailer;
	
	class Sedan : public Vehicle, IDrivable
	{
	public:
		Sedan();
		virtual ~Sedan();

		Sedan(const Sedan& rhs);
		Sedan& operator=(const Sedan& other);

		bool AddTrailer(const Trailer* trailer);
		bool RemoveTrailer();

		virtual unsigned int GetMaxSpeed() const;
		virtual unsigned int GetDriveSpeed() const;
		virtual void ExecuteTravel(unsigned int index) const;

	private:
		const Trailer* mTrailer;
		bool mbTravelPlan[6];
		bool mbTravelPlanTrailer[7];
	};
}