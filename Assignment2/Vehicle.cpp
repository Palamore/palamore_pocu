#include "Vehicle.h"

namespace assignment2
{
	Vehicle::Vehicle(unsigned int maxPassengersCount)
		: mMaxPassengersCount(maxPassengersCount)
		, mPassengersCount(0)
		, mEulerNumb(2.718)
	{
		mPassengers = new const Person * [maxPassengersCount];
		for (size_t i = 0; i < maxPassengersCount; i++)
		{
			mPassengers[i] = nullptr;
		}
	}

	Vehicle::~Vehicle()
	{
		for (size_t i = 0; i < mPassengersCount; i++)
		{
			delete mPassengers[i];
		}
		delete[] mPassengers; 
	}

	Vehicle::Vehicle(const Vehicle& other)
		: mMaxPassengersCount(other.GetMaxPassengersCount())
		, mPassengersCount(other.GetPassengersCount())
		, mEulerNumb(other.mEulerNumb)
	{
		mPassengers = new const Person * [other.GetMaxPassengersCount()];
		for (size_t i = 0; i < other.GetPassengersCount(); i++)
		{
			mPassengers[i] = new Person(other.GetPassenger(i)->GetName(), other.GetPassenger(i)->GetWeight());
		}
	}

	Vehicle& Vehicle::operator=(const Vehicle& other)
	{
		
		if (this->mPassengers == other.mPassengers)
		{
			return *this;
		}
		

		const Person** tmpPassengers = new const Person * [other.GetMaxPassengersCount()];
		for (size_t i = 0; i < other.GetPassengersCount(); i++)
		{
			tmpPassengers[i] = new Person(other.mPassengers[i]->GetName(), other.mPassengers[i]->GetWeight());
		}

		for (size_t i = 0; i < mPassengersCount; i++)
		{
			delete mPassengers[i];
		}

		delete[] mPassengers;

		mPassengers = tmpPassengers;

		mMaxPassengersCount = other.GetMaxPassengersCount();
		mPassengersCount = other.GetPassengersCount();
		mEulerNumb = other.mEulerNumb;
		return *this;
	}


	bool Vehicle::AddPassenger(const Person* person)
	{
		for (size_t i = 0; i < mPassengersCount ; i++)
		{
			if (person == mPassengers[i])
			{
				return false;
			}
		}
		if (mPassengersCount == mMaxPassengersCount || person == NULL)
		{
			return false;
		}

		mPassengers[mPassengersCount] = person;
		mPassengersCount++;

		return true;
	}

	bool Vehicle::RemovePassenger(unsigned int i)
	{
		if (i >= mPassengersCount || i < 0)
		{
			return false;
		}

		delete mPassengers[i];
		for (size_t j = i; j < mPassengersCount - 1; j++)
		{
			mPassengers[j] = mPassengers[j + 1];
		}
		mPassengers[mPassengersCount - 1] = nullptr;
		mPassengersCount--;
		return true;
	}

	unsigned int Vehicle::GetPassengersCount() const
	{
		return mPassengersCount;
	}

	unsigned int Vehicle::GetMaxPassengersCount() const
	{
		return mMaxPassengersCount;
	}

	const Person* Vehicle::GetPassenger(unsigned int i) const
	{
		if (i >= mPassengersCount || i < 0)
		{
			return NULL;
		}
		return mPassengers[i];
	}




	unsigned int Vehicle::getPassengersWeight() const
	{

		unsigned int sum = 0;
		for (size_t i = 0; i < mPassengersCount; i++)
		{
			sum += mPassengers[i]->GetWeight();
		}

		return sum;
	}

}