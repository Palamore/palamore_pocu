#include "Boatplane.h"

namespace assignment2
{
	Boatplane::Boatplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		mbTravelPlan[0] = true;
		mbTravelPlan[1] = false;
		mbTravelPlan[2] = false;
		mbTravelPlan[3] = false;

	}

	Boatplane::~Boatplane()
	{

	}

	Boatplane::Boatplane(unsigned int maxPassengersCount, unsigned int passengersCount, const Person** passengers) 
		: Vehicle(maxPassengersCount)
	{
		mbTravelPlan[0] = true;
		mbTravelPlan[1] = false;
		mbTravelPlan[2] = false;
		mbTravelPlan[3] = false;
		delete[] mPassengers;
		mPassengers = passengers;
		mPassengersCount = passengersCount;

	}

	unsigned int Boatplane::GetMaxSpeed() const
	{
		if (GetFlySpeed() > GetSailSpeed())
		{
			return GetFlySpeed();
		}
		return GetSailSpeed();
	}

	unsigned int Boatplane::GetFlySpeed() const
	{
		double result = 0.0;
		result = 150 * (pow(mEulerNumb, (500.0 - static_cast<double>(getPassengersWeight())) / 300.0));

		unsigned int flySpeed = static_cast<unsigned int>(round(result));

		return flySpeed;
	}

	unsigned int Boatplane::GetSailSpeed() const
	{
		double result = 800.0 - (1.7 * static_cast<double>(getPassengersWeight()));
		if (result < 20.0)
		{
			return static_cast<unsigned int>(20);
		}
		unsigned int sailSpeed = static_cast<unsigned int>(round(result));

		return sailSpeed;
	}

	void Boatplane::ExecuteTravel(unsigned int index) const
	{
		unsigned int travelCount = DeusExMachina::GetInstance()->TravelCount[index];
		while (travelCount >= 4)
		{
			travelCount -= 4;
		}
		if (mbTravelPlan[travelCount])
		{
			DeusExMachina::GetInstance()->TravelledDistanceHolder[index] += GetMaxSpeed();
		}
	}


}