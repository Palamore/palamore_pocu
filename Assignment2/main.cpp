#include "Person.h"
#include "Vehicle.h"
#include "Airplane.h"
#include "Boat.h"
#include "Boatplane.h"
#include "DeusExMachina.h"
#include "Sedan.h"
#include "Motorcycle.h"
#include "Trailer.h"
#include "UBoat.h"
#include <iostream>
#include <cassert>
#include <crtdbg.h>

#define STR(name) #name

using namespace assignment2;
using namespace std;

int main()
{


	Airplane mAir(3);

	
//	_CrtSetBreakAlloc(165);
	/*
	Airplane mAir(3);
	mAir.AddPassenger(new Person("Tim", 50));

	mAir = mAir;

	Airplane nAir = mAir;


	Boat mBoat(3);
	Boatplane mBp = mAir + mBoat;  // 최대6명
	Motorcycle mMotor;  // 최대2명 
	Sedan mSedan1; // 최대4명 + 트레일러
	Sedan mSedan2;
	Trailer* mTr = new Trailer(50);
	mSedan2.AddTrailer(mTr);
	UBoat mUBoat; // 최대 50명





//	*/



	/*

	UBoat mUBoat;
	mUBoat.AddPassenger(new Person("UTim", 50));
	UBoat nUBoat;
	nUBoat = mUBoat;
	UBoat* npUBoat = new UBoat(nUBoat);
	delete npUBoat;

	Motorcycle mMotor;
	mMotor.AddPassenger(new Person("MTim", 50));
	Motorcycle nMotor;
	nMotor = mMotor;
	Motorcycle* npMotor = new Motorcycle(nMotor);
	delete npMotor;

	Boatplane mBp(10);
	mBp.AddPassenger(new Person("BPTim", 50));
	Boatplane nBp(7);
	nBp = mBp;
	Boatplane* npBp = new Boatplane(nBp);
	delete npBp;

	Boat mBoat(5);
	mBoat.AddPassenger(new Person("BTim", 50));
	Boat nBoat(5);
	nBoat = mBoat;
	Boat* npBoat = new Boat(nBoat);
	delete npBoat;


	Airplane mAir(5);
	mAir.AddPassenger(new Person("ATim", 50));
	Airplane nAir(5);
	nAir = mAir;
	Airplane* npAir = new Airplane(nAir);
	delete npAir;


	Sedan mSedan;
	mSedan.AddPassenger(new Person("Tim", 50));
	Trailer* mTr = new Trailer(50);
	mSedan.AddTrailer(mTr);
	Sedan nSedan;
	nSedan = mSedan;
	Sedan* npSedan = new Sedan(nSedan);
	delete npSedan;
	delete DeusExMachina::GetInstance();
	
//	/*

//	/*
	DeusExMachina* d = DeusExMachina::GetInstance();
	Vehicle* demAirplane = new Airplane(10);
	Vehicle* demBoat = new Boat(5);
	Vehicle* demBoatplane = new Boatplane(15);
	Vehicle* demMotorcycle = new Motorcycle();
	Vehicle* demSedan1 = new Sedan();
	Vehicle* demSedan2 = new Sedan();
	Vehicle* demUBoat = new UBoat();
	Trailer* demTrailer = new Trailer(10);
	static_cast<Sedan*>(demSedan2)->AddTrailer(demTrailer);

	Person* demPerson;
	for (int i = 0; i < 3; i++)
	{
		demPerson = new Person("Airp", 30);
		cout << i << " : " << demAirplane->AddPassenger(demPerson) << endl;
	}
	for (int i = 3; i < 6; i++)
	{
		demPerson = new Person("Boat", 30);
		cout << i << " : " << demBoat->AddPassenger(demPerson) << endl;
	}
	for (int i = 6; i < 9; i++)
	{
		demPerson = new Person("Boap", 30);
		cout << i << " : " << demBoatplane->AddPassenger(demPerson) << endl;
	}
	for (int i = 9; i < 12; i++)
	{
		demPerson = new Person("Moto", 30);
		cout << i << " : " << demMotorcycle->AddPassenger(demPerson) << endl;
	}
	for (int i = 12; i < 15; i++)
	{
		demPerson = new Person("Sed1", 30);
		cout << i << " : " << demSedan1->AddPassenger(demPerson) << endl;
	}
	for (int i = 15; i < 18; i++)
	{
		demPerson = new Person("Sed2", 30);
		cout << i << " : " << demSedan2->AddPassenger(demPerson) << endl;
	}
	for (int i = 18; i < 21; i++)
	{
		demPerson = new Person("UBot", 30);
		cout << i << " : " << demUBoat->AddPassenger(demPerson) << endl;
	}

	cout << demAirplane->GetMaxSpeed() << endl;
	cout << demBoat->GetMaxSpeed() << endl;
	cout << demBoatplane->GetMaxSpeed() << endl;
	cout << demMotorcycle->GetMaxSpeed() << endl;
	cout << demSedan1->GetMaxSpeed() << endl;
	cout << demSedan2->GetMaxSpeed() << endl;
	cout << demUBoat->GetMaxSpeed() << endl;

	d->AddVehicle(demAirplane);
	d->AddVehicle(demBoat);
	d->AddVehicle(demBoatplane);
	d->AddVehicle(demMotorcycle);
	d->AddVehicle(demSedan1);
	d->AddVehicle(demSedan2);
	d->AddVehicle(demUBoat);

	cout << demAirplane->GetPassengersCount() << endl;
	cout << "Airp Passenger Remove 1: " << demAirplane->RemovePassenger(0) << endl;
	cout << demAirplane->GetPassengersCount() << endl;
	cout << "Airp Passenger Remove 0: " << demAirplane->RemovePassenger(2) << endl;
	cout << demAirplane->GetPassengersCount() << endl;

	cout << "Try Remove Trailer Sedan1 :" << static_cast<Sedan*>(demSedan1)->RemoveTrailer() << endl;
	cout << "Try Remove Trailer Sedan2 :" << static_cast<Sedan*>(demSedan2)->RemoveTrailer() << endl;


	delete d;
	*/
	/*
	Vehicle* a = new Airplane(5);

	a->AddPassenger(new Person("Timmy", 50));

	cout << a->GetPassengersCount() << endl;
	cout << a->GetMaxSpeed() << endl;
	cout << a->GetPassenger(0)->GetName() << endl;

	const Person* b = a->GetPassenger(0);

	a = a;

	cout << a->GetPassengersCount() << endl;
	cout << a->GetMaxSpeed() << endl;
	cout << a->GetPassenger(0)->GetName() << endl;

	const Person* c = a->GetPassenger(0);

	Airplane aa(5);

	aa.AddPassenger(new Person("Tim", 50));

	cout << aa.GetPassengersCount() << endl;
	cout << aa.GetMaxSpeed() << endl;
	cout << aa.GetPassenger(0)->GetName() << endl;

	const Person* bb = aa.GetPassenger(0);

	aa = aa;

	cout << aa.GetPassengersCount() << endl;
	cout << aa.GetMaxSpeed() << endl;
	cout << aa.GetPassenger(0)->GetName() << endl;

	const Person* cc = aa.GetPassenger(0);

	if (bb == cc)
	{
		cout << "It's also same" << endl;
	}


	delete a;
	*/


	/*
	const char* MAX_SPEED_LABLE = "Max Speed: ";
	const char* CUR_P_LABLE = "Current Person: ";
	const unsigned int MAX_CAPACITY = 10;

	Vehicle* air = new Airplane(MAX_CAPACITY);

	Person* toAdd;
	const unsigned int personWeight = 10;

	for (size_t i = 0; i < MAX_CAPACITY + 10; i++)
	{
		toAdd = new Person(STR(i), i);
		if (air->AddPassenger(toAdd) == false)
		{
			delete toAdd;
		}

		cout << MAX_SPEED_LABLE << air->GetMaxSpeed() << endl
			<< CUR_P_LABLE << air->GetPassengersCount() << endl;
	}

	while (air->RemovePassenger(0))
	{
		cout << CUR_P_LABLE << air->GetPassengersCount() << endl;;
	}

	Person* overlapTest = new Person("Overlap Test", 100);
	air->AddPassenger(overlapTest);
	air->AddPassenger(overlapTest);
	assert(air->GetPassengersCount() == 1);

	toAdd = NULL;
	assert(air->AddPassenger(toAdd) == false);

	delete air;

	Airplane dockingTest1(10);
	Boat dockingTest2(10);

	for (size_t i = 0; i < 5; i++)
	{
		dockingTest1.AddPassenger(new Person(STR(i), i));
		dockingTest2.AddPassenger(new Person(STR(i), i));
	}

	const Person* comp1 = dockingTest1.GetPassenger(0);


	Boatplane bp2 = dockingTest2 + dockingTest1;
	Boatplane bp1 = dockingTest1 + dockingTest2;

	const Person* comp2 = bp2.GetPassenger(0);

	assert(comp1 == comp2);
	assert(dockingTest1.GetPassengersCount() == 0);
	assert(dockingTest2.GetPassengersCount() == 0);
	assert(bp2.GetPassengersCount() == 10);
	assert(bp1.GetPassengersCount() == 0);

	Boatplane copyConstuctorTest(bp2);

	for (size_t i = 0; i < bp1.GetPassengersCount(); i++)
	{
		const Person* p1 = bp1.GetPassenger(i);
		const Person* p2 = copyConstuctorTest.GetPassenger(i);
		assert(p1 != p2);
	}
	assert(bp2.GetMaxPassengersCount() == copyConstuctorTest.GetMaxPassengersCount());
	assert(bp2.GetPassengersCount() == copyConstuctorTest.GetPassengersCount());
	assert(bp2.GetMaxSpeed() == copyConstuctorTest.GetMaxSpeed());

	Sedan sedanTest;
	Trailer* t1 = new Trailer(10);
	Trailer* t2 = new Trailer(20);

	assert(sedanTest.AddTrailer(t1));
	assert(!sedanTest.AddTrailer(t1));
	assert(!sedanTest.AddTrailer(t2));
	assert(sedanTest.RemoveTrailer());
	assert(sedanTest.AddTrailer(t2));
	assert(sedanTest.RemoveTrailer());

	DeusExMachina* d = DeusExMachina::GetInstance();
	Vehicle* demAirplain = new Airplane(MAX_CAPACITY);
	Vehicle* demBoat = new Airplane(MAX_CAPACITY);
	Vehicle* demBoatplain = new Boatplane(MAX_CAPACITY);
	Vehicle* demMotorcycle = new Motorcycle();
	Vehicle* demSedan1 = new Sedan();
	Vehicle* demSedan2 = new Sedan();
	Trailer* demTrailer = new Trailer(10);
	static_cast<Sedan*>(demSedan2)->AddTrailer(demTrailer);
	Vehicle* demUBoat = new UBoat();

	d->AddVehicle(demAirplain);
	d->AddVehicle(demBoat);
	d->AddVehicle(demBoatplain);
	d->AddVehicle(demMotorcycle);
	d->AddVehicle(demSedan1);
	d->AddVehicle(demSedan2);
	d->AddVehicle(demUBoat);

	for (size_t i = 0; i < 7; i++)
	{
		Vehicle* tempVPointer = d->GetVehicle(i);
		for (size_t j = tempVPointer->GetPassengersCount(); j < tempVPointer->GetMaxPassengersCount(); j++)
		{
			tempVPointer->AddPassenger(new Person(STR((i + j)), 10));
		}
	}


	for (size_t i = 0; i < 10; i++)
	{
		d->Travel();
	}

	delete d;
//	*/

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	return 0;
}