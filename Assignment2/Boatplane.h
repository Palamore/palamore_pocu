#pragma once

#include "Vehicle.h"
#include "IFlyable.h"
#include "ISailable.h"
#include "DeusExMachina.h"

namespace assignment2
{
	class Boatplane : public Vehicle, IFlyable, ISailable
	{
	public:
		Boatplane(unsigned int maxPassengersCount);
		virtual ~Boatplane();


		Boatplane(unsigned int maxPassengersCount, unsigned int passengersCount, const Person** passengers);


		virtual unsigned int GetMaxSpeed() const;
		virtual unsigned int GetFlySpeed() const;
		virtual unsigned int GetSailSpeed() const;
		virtual void ExecuteTravel(unsigned int index) const;

	private:
		bool mbTravelPlan[4];
	};
}