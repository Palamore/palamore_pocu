#pragma once

#include "Vehicle.h"
#include "IDivable.h"
#include "ISailable.h"
#include "DeusExMachina.h"

namespace assignment2
{
	class UBoat : public Vehicle, IDivable, ISailable
	{
	public:
		UBoat();
		~UBoat();

		virtual unsigned int GetMaxSpeed() const;
		virtual unsigned int GetDiveSpeed() const;
		virtual unsigned int GetSailSpeed() const;
		virtual void ExecuteTravel(unsigned int index) const;
	private:
		bool mbTravelPlan[6];
	};
}