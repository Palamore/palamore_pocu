#include <iostream>
#include <iomanip>
#include "Add.h"
using namespace std;
#define SWAP(a, b) ( t = a )
/*
void PrintMenuExample() {
	const float coffeePrice = 1.25f;
	const float lattePrice = 4.75f;

	const size_t nameColumnLength = 20;
	const size_t priceColumnLength = 10;

	cout << left << fixed << showpoint << setprecision(2);
	cout << setfill('-') << setw(nameColumnLength + priceColumnLength)
		<< "" << endl << setfill(' ');
	cout << setw(nameColumnLength) << "Name"
		<< setw(priceColumnLength) << "Price" << endl;
	cout << setfill('-') << setw(nameColumnLength + priceColumnLength)
		<< "" << endl << setfill(' ');
	cout << setw(nameColumnLength) << "Coffee" << "$" << coffeePrice << endl;

}
*/
const int LINE_SIZE = 512;
char line[LINE_SIZE];

int main()
{
	int temp;
	temp = lab1::Add(100, 23);

	cout << hex << temp << endl;

	cin.getline(line, LINE_SIZE);

	if (cin.fail()) 
	{
		cin.clear();
		return 0;
	}

	char* a = line;
	char* b = line + strlen(line) - 1;
	while (a < b) {
		char temp = *a;
		*a = *b;
		*b = temp;

		a++;
		b--;
	}

	cout << line << endl;




	
	return 0;
}