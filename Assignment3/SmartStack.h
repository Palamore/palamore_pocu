#pragma once

#include <stack>
#include <limits>

namespace assignment3
{
	template <typename T>
	class SmartStack
	{
	public:
		SmartStack();
		virtual ~SmartStack();

		void Push(const T& number);
		T Pop();
		T Peek() const;
		T GetMax();
		T GetMin();
		double GetAverage();
		T GetSum();
		double GetVariance();
		double GetStandardDeviation();
		unsigned int GetCount() const;


	private:
		std::stack<T> mStack;
		std::stack<T> mMinStack;
		//최초 1회 숫자와 그 이하의 숫자가 들어올 때마다 스택.
		std::stack<T> mMaxStack;
		//최초 1회 숫자와 그 이상의 숫자가 들어올 때마다 스택.
		unsigned int mSize;
		double mSum;
		double mSqrSum; // 제곱의 합
	};

	template <typename T>
	SmartStack<T>::SmartStack()
		: mSize(0)
		, mSum(0.0)
		, mSqrSum(0.0)
	{

	}

	template <typename T>
	SmartStack<T>::~SmartStack()
	{

	}

	template <typename T>
	void SmartStack<T>::Push(const T& number)
	{
		double tempNumber = static_cast<double>(number);
		mStack.push(number);
		mSum += tempNumber;
		mSqrSum += tempNumber * tempNumber;
		if (mSize == 0)
		{
			mMinStack.push(number);
			mMaxStack.push(number);
			mSize++;
			return;
		}
		if (mMinStack.top() >= number)
		{
			mMinStack.push(number);
		}
		if (mMaxStack.top() <= number)
		{
			mMaxStack.push(number);
		}
		mSize++;
	}

	template <typename T>
	T SmartStack<T>::Pop()
	{
		T temp = mStack.top();
		double tempNumber = static_cast<double>(temp);
		mStack.pop();
		mSum -= tempNumber;
		mSqrSum -= tempNumber * tempNumber;
		mSize--;
		if (mMinStack.top() == temp)
		{
			mMinStack.pop();
		}
		if (mMaxStack.top() == temp)
		{
			mMaxStack.pop();
		}
		return temp;
	}

	template <typename T>
	T SmartStack<T>::Peek() const
	{
		return mStack.top();
	}

	template <typename T>
	T SmartStack<T>::GetMax()
	{
		T temp = std::numeric_limits<T>().lowest();
		if (mSize != 0)
		{
			temp = mMaxStack.top();
		}
		return temp;
	}

	template <typename T>
	T SmartStack<T>::GetMin()
	{
		T temp = std::numeric_limits<T>().max();
		if (mSize != 0)
		{
			temp = mMinStack.top();
		}
		return temp;
	}

	template <typename T>
	double SmartStack<T>::GetAverage()
	{
		double result = mSum / static_cast<double>(mSize);
		result = round(result * 1000.0) / 1000.0;
		return result;
	}

	template <typename T>
	T SmartStack<T>::GetSum()
	{
		if (mSize == 0)
		{
			mSum = 0.0;
			return static_cast<T>(mSum);
		}
		return static_cast<T>(mSum);
	}

	template <typename T>
	double SmartStack<T>::GetVariance()
	{
		if (mSize == 0)
		{
			return 0.0;
		}
		double size = static_cast<double>(mSize);
		double result = (mSqrSum / size) - ((mSum / size) * (mSum / size));
		result = round(result * 1000.0) / 1000.0;
		return result;
	}

	template <typename T>
	double SmartStack<T>::GetStandardDeviation()
	{
		if (mSize == 0)
		{
			return 0.0;
		}
		double result = GetVariance();
		result = sqrt(result);
		result = round(result * 1000.0) / 1000.0;
		return result;
	}

	template <typename T>
	unsigned int SmartStack<T>::GetCount() const
	{
		return mSize;
	}

}