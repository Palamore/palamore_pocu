#include "SmartStack.h"
#include "SmartQueue.h"
#include "QueueStack.h"
#include <iostream>
#include <cassert>
#include <crtdbg.h>
using namespace std;
using namespace assignment3;
template <typename T>
class Tester
{
public:
	template<typename T>
	Tester(T anyNumber)
	{
		std::cout << std::numeric_limits<T>().max() << std::endl;
		std::cout << std::numeric_limits<T>().lowest() << std::endl;
	}
};



int main()
{
	/*


	SmartStack<double> ss;

	ss.Push(3.4);
	ss.Push(1.2);
	ss.Push(4.6);
	ss.Push(3.32);
	ss.Push(10.2);
	ss.Push(1.1);
	ss.Push(-5.9);
	ss.Push(1.1);
	ss.Push(-12.4);
	ss.Push(9.2);

	assert(ss.GetCount() == 10U);
	assert(ss.Peek() == 9.2);
	assert(ss.GetMax() == 10.2);
	assert(ss.GetMin() == -12.4);
	assert(ss.GetSum() == 15.82);
	assert(ss.GetAverage() == 1.582);
	assert(ss.GetVariance() == 39.983);
	assert(ss.GetStandardDeviation() == 6.323);
	assert(ss.Peek() == 9.2);

	double popped1 = ss.Pop();
	double popped2 = ss.Pop();

	assert(popped1 == 9.2);
	assert(popped2 == -12.4);


	SmartQueue<double> sq;

	sq.Enqueue(3.4);
	sq.Enqueue(1.2);
	sq.Enqueue(4.6);
	sq.Enqueue(3.32);
	sq.Enqueue(10.2);
	sq.Enqueue(1.1);
	sq.Enqueue(-5.9);
	sq.Enqueue(1.1);
	sq.Enqueue(-12.4);
	sq.Enqueue(9.2);

	assert(sq.GetCount() == 10U);
	assert(sq.Peek() == 3.4);
	assert(sq.GetMax() == 10.2);
	assert(sq.GetMin() == -12.4);
	assert(sq.GetSum() == 15.82);
	assert(sq.GetAverage() == 1.582);
	assert(sq.GetVariance() == 39.983);
	assert(sq.GetStandardDeviation() == 6.323);
	assert(sq.Peek() == 3.4);

	double popped11 = sq.Dequeue();
	double popped22 = sq.Dequeue();

	assert(popped11 == 3.4);
	assert(popped22 == 1.2);
	*/
	// 디버그시작점	
//	/*
	SmartStack<float> ss;

	ss.Push(3.4999f);
	ss.Push(1.2f);
	ss.Push(4.6555f);
	ss.Push(3.3299f);
	ss.Push(10.2f);
	ss.Push(1.1f);
	ss.Push(-5.9f);
	ss.Push(1.1f);
	ss.Push(-12.4f);
	ss.Push(9.2f);

	assert(ss.GetCount() == 10U);
	assert(ss.Peek() == 9.2f);
	assert(ss.GetMax() == 10.2f);
	assert(ss.GetMin() == -12.4f);
//	assert(ss.GetSum() == 15.985301f);
	assert(ss.GetAverage() == 1.599);
	assert(ss.GetVariance() == 40.057);
	assert(ss.GetStandardDeviation() == 6.329);
	assert(ss.Peek() == 9.2f);

	float popped1 = ss.Pop();
	float popped2 = ss.Pop();

	assert(popped1 == 9.2f);
	assert(popped2 == -12.4f);
	assert(ss.GetCount() == 8U);
	assert(ss.Peek() == 1.1f);
	assert(ss.GetMax() == 10.2f);
	assert(ss.GetMin() == -5.9f);
//	assert(ss.GetSum() == 19.1853008f);
	assert(ss.GetAverage() == 2.398);
	assert(ss.GetVariance() == 17.714);
	assert(ss.GetStandardDeviation() == 4.209);

	SmartQueue<float> sq;

	sq.Enqueue(3.4999f);
	sq.Enqueue(1.2f);
	sq.Enqueue(4.6555f);
	sq.Enqueue(3.3299f);
	sq.Enqueue(10.2f);
	sq.Enqueue(1.1f);
	sq.Enqueue(-5.9f);
	sq.Enqueue(1.1f);
	sq.Enqueue(-12.4f);
	sq.Enqueue(9.2f);

	assert(sq.GetCount() == 10U);
	assert(sq.Peek() == 3.4999f);
	assert(sq.GetMax() == 10.2f);
	assert(sq.GetMin() == -12.4f);
//	assert(sq.GetSum() == 15.9853010f);
	assert(sq.GetAverage() == 1.599);
	assert(sq.GetVariance() == 40.057);
	assert(sq.GetStandardDeviation() == 6.329);
	assert(sq.Peek() == 3.4999f);

	float dequeued1 = sq.Dequeue();
	float dequeued2 = sq.Dequeue();

	assert(dequeued1 == 3.4999f);
	assert(dequeued2 == 1.2f);
	assert(sq.GetCount() == 8U);
	assert(sq.Peek() == 4.6555f);
	assert(sq.GetMax() == 10.2f);
	assert(sq.GetMin() == -12.4f);
//	assert(sq.GetSum() == 11.2854013f);
	assert(sq.GetAverage() == 1.411);
	assert(sq.GetVariance() == 49.564);
	assert(sq.GetStandardDeviation() == 7.040);
	

	QueueStack<float> qs(3);

	qs.Enqueue(3.4f);
	qs.Enqueue(1.2f);
	qs.Enqueue(4.6f);
	qs.Enqueue(3.32f);
	qs.Enqueue(10.2f);
	qs.Enqueue(1.1f);
	qs.Enqueue(-5.9f);
	qs.Enqueue(1.1f);
	qs.Enqueue(-12.4f);
	qs.Enqueue(9.2f);

	assert(qs.GetStackCount() == 4U);
	assert(qs.Peek() == 4.6f);
	assert(qs.GetCount() == 10U);
	assert(qs.GetMax() == 10.2f);
	assert(qs.GetMin() == -12.4f);
	assert(qs.GetSum() == 15.8200026f);
	assert(qs.GetAverage() == 1.582);
	assert(qs.GetStackCount() == 4U);
	assert(qs.Peek() == 4.6f);

	float dequeued3 = qs.Dequeue();
	float dequeued4 = qs.Dequeue();
	float dequeued5 = qs.Dequeue();

	assert(dequeued3 == 4.6f);
	assert(dequeued4 == 1.2f);
	assert(dequeued5 == 3.4f);
	assert(qs.GetCount() == 7U);
	assert(qs.GetStackCount() == 3U);
	assert(qs.GetMax() == 10.2f);
	assert(qs.GetMin() == -12.4f);
	assert(qs.GetSum() == 6.62000322f);
	assert(qs.GetAverage() == 0.946);
//	*/

	/*
	SmartStack<double> stDouble;
	stDouble.Push(3.4);
	stDouble.Push(1.2);
	stDouble.Push(4.6);
	stDouble.Push(3.32);
	stDouble.Push(10.2);
	stDouble.Push(1.1);
	stDouble.Push(-5.9);
	stDouble.Push(1.1);
	stDouble.Push(-12.4);
	stDouble.Push(9.2);

	SmartStack<float> stFloat;
	stFloat.Push(3.4f);
	stFloat.Push(1.2f);
	stFloat.Push(4.6f);
	stFloat.Push(3.32f);
	stFloat.Push(10.2f);
	stFloat.Push(1.1f);
	stFloat.Push(-5.9f);
	stFloat.Push(1.1f);
	stFloat.Push(-12.4f);
	stFloat.Push(9.2f);

	SmartStack<int> stInt;
	stInt.Push(3);
	stInt.Push(1);
	stInt.Push(4);
	stInt.Push(3);
	stInt.Push(10);
	stInt.Push(1);
	stInt.Push(-6);
	stInt.Push(1);
	stInt.Push(-12);
	stInt.Push(9);

	SmartStack<short> stShort;
	stShort.Push(3);
	stShort.Push(1);
	stShort.Push(4);
	stShort.Push(3);
	stShort.Push(10);
	stShort.Push(1);
	stShort.Push(-6);
	stShort.Push(1);
	stShort.Push(-12);
	stShort.Push(9);

	SmartStack<long> stLong;
	stLong.Push(3);
	stLong.Push(1);
	stLong.Push(4);
	stLong.Push(3);
	stLong.Push(10);
	stLong.Push(1);
	stLong.Push(-6);
	stLong.Push(1);
	stLong.Push(-12);
	stLong.Push(9);



	cout << stDouble.GetVariance() << endl;
	cout << stFloat.GetVariance() << endl;
	cout << stInt.GetVariance() << endl;
	cout << stShort.GetVariance() << endl;
	cout << stLong.GetVariance() << endl;
	
	QueueStack<float> qs(3);

	qs.Enqueue(3.4f);
	qs.Enqueue(1.2f);
	qs.Enqueue(4.6f);
	qs.Enqueue(3.32f);
	qs.Enqueue(10.2f);
	qs.Enqueue(1.1f);
	qs.Enqueue(-5.9f);
	qs.Enqueue(1.1f);
	qs.Enqueue(-12.4f);
	qs.Enqueue(9.2f);

	QueueStack<float> qs2(3);

	qs = qs;

	qs2 = qs;

	cout << qs.GetSum() << endl;
	cout << qs2.GetSum() << endl;


	for (int i = 0; i < 10; i++)
	{
		cout << qs.Dequeue() << endl;
	}

	cout << "-----------------------------" << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << qs2.Dequeue() << endl;
	}
	*/



	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	return 0;
}