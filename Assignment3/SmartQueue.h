#pragma once

#include <queue>
#include <limits>

namespace assignment3
{
	template <typename T>
	class SmartQueue
	{
	public:
		SmartQueue();
		virtual ~SmartQueue();



		void Enqueue(const T& number);
		T Peek() const;
		T Dequeue();
		T GetMax();
		T GetMin();
		double GetAverage();
		T GetSum();
		double GetVariance();
		double GetStandardDeviation();
		unsigned int GetCount() const;
	private:
		std::queue<T> mQueue;
		double mSum;
		double mSqrSum;
	};

	template <typename T>
	SmartQueue<T>::SmartQueue()
		: mSum(0.0)
		, mSqrSum(0.0)
	{
		
	}

	template <typename T>
	SmartQueue<T>::~SmartQueue()
	{

	}
	/*
	template <typename T>
	SmartQueue<T>::SmartQueue(const SmartQueue<T>& other)
	{

	}

	template <typename T>
	SmartQueue<T>& SmartQueue<T>::operator=(const SmartQueue<T>& other)
	{
		

	}
	*/
	template <typename T>
	void SmartQueue<T>::Enqueue(const T& number)
	{
		mQueue.push(number);
		double tempNumber = static_cast<double>(number);
		mSum += tempNumber;
		mSqrSum += tempNumber * tempNumber;

	}

	template <typename T>
	T SmartQueue<T>::Peek() const
	{
		return mQueue.front();
	}

	template <typename T>
	T SmartQueue<T>::Dequeue()
	{
		T temp = mQueue.front();
		double tempNumber = static_cast<double>(temp);
		mQueue.pop();
		mSum -= tempNumber;
		mSqrSum -= tempNumber * tempNumber;
		return temp;
	}

	template <typename T>
	T SmartQueue<T>::GetMax()
	{
		if (mQueue.size() == 0)
		{
			return std::numeric_limits<T>().lowest();
		}
		std::queue<T> tempQ = mQueue;
		T tempV = tempQ.front();
		tempQ.pop();
		unsigned int sizeHolder = tempQ.size();
		for (size_t i = 0; i < sizeHolder; i++)
		{
			if (tempV < tempQ.front())
			{
				tempV = tempQ.front();
			}
			tempQ.pop();
		}
		return tempV;
	}

	template <typename T>
	T SmartQueue<T>::GetMin()
	{
		if (mQueue.size() == 0)
		{
			return std::numeric_limits<T>().max();
		}
		std::queue<T> tempQ = mQueue;
		T tempV = tempQ.front();
		tempQ.pop();
		unsigned int sizeHolder = tempQ.size();
		for (size_t i = 0; i < sizeHolder; i++)
		{
			if (tempV > tempQ.front())
			{
				tempV = tempQ.front();
			}
			tempQ.pop();
		}
		return tempV;
	}

	template <typename T>
	double SmartQueue<T>::GetAverage()
	{
		double result = mSum / static_cast<double>(mQueue.size());
		result = round(result * 1000.0) / 1000.0;
		return result;
	}

	template <typename T>
	T SmartQueue<T>::GetSum()
	{
		if (mQueue.size() == 0)
		{
			mSum = 0.0;
			return static_cast<T>(mSum);
		}
		/*
		double temp = static_cast<double>(mSum);
		temp = round(temp * 1000.0) / 1000.0;
		return static_cast<T>(temp);
		*/
		return static_cast<T>(mSum);
	}

	template <typename T>
	double SmartQueue<T>::GetVariance()
	{
		if (mQueue.size() == 0)
		{
			return 0.0;
		}
		double size = static_cast<double>(mQueue.size());
		double result = (mSqrSum / size) - ((mSum / size) * (mSum / size));
		result = round(result * 1000.0) / 1000.0;
		return result;

	}

	template <typename T>
	double SmartQueue<T>::GetStandardDeviation()
	{
		if (mQueue.size() == 0)
		{
			return 0.0;
		}
		/*
		double sqrSum = static_cast<double>(mSqrSum);
		double size = static_cast<double>(mQueue.size());
		double sum = static_cast<double>(mSum);
		double result = (sqrSum / size) - ((sum / size) * (sum / size));
		result = sqrt(result);
		*/
		double result = GetVariance();
		result = sqrt(result);
		result = round(result * 1000.0) / 1000.0;
		return result;
	}

	template <typename T>
	unsigned int SmartQueue<T>::GetCount() const
	{
		return mQueue.size();
	}

}