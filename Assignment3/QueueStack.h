#pragma once

#include <queue>
#include <stack>
#include <limits>

namespace assignment3
{
	template <typename T>
	class QueueStack
	{
	public:
		QueueStack(unsigned int maxStackSize);
		virtual ~QueueStack();

		QueueStack(const QueueStack<T>& other);
		QueueStack& operator=(const QueueStack<T>& other);

		void Enqueue(const T& number);
		T Peek() const;
		T Dequeue();
		T GetMax();
		T GetMin();
		double GetAverage();
		T GetSum();
		unsigned int GetCount() const;
		unsigned int GetStackCount() const;


	private:
		std::queue<std::stack<T>*> mQueue;
		std::stack<T>* mStackPointer;
		T mSum;
		unsigned int mMaxStackSize;
	};

	template <typename T>
	QueueStack<T>::QueueStack(unsigned int maxStackSize)
		: mMaxStackSize(maxStackSize)
		, mSum(0)
		, mStackPointer(nullptr)
	{

	}

	template <typename T>
	QueueStack<T>::~QueueStack()
	{
		unsigned int sizeHolder = mQueue.size();
		for (size_t i = 0; i < sizeHolder; i++)
		{
			std::stack<T>* stackPointer;
			stackPointer = mQueue.front();
			mQueue.pop();
			delete stackPointer;
		}
	}

	template <typename T>
	QueueStack<T>::QueueStack(const QueueStack<T>& other)
		: mMaxStackSize(other.mMaxStackSize)
		, mSum(other.mSum)
		, mStackPointer(nullptr)
	{
		std::stack<T> tempStack;
		std::queue<std::stack<T>*> tempQueue = other.mQueue;
		for (size_t i = 0; i < other.mQueue.size(); i++)
		{
			tempStack = *tempQueue.front();
			mStackPointer = new std::stack<T>(tempStack);
			mQueue.push(mStackPointer);
			tempQueue.pop();
		}

	}

	template <typename T>
	QueueStack<T>& QueueStack<T>::operator=(const QueueStack<T>& other)
	{
		if (mStackPointer == other.mStackPointer)
		{
			mSum = other.mSum;
			mMaxStackSize = other.mMaxStackSize;
			return *this;
		}

		unsigned int sizeHolder = mQueue.size();
		for (size_t i = 0; i < sizeHolder; i++)
		{
			mStackPointer = mQueue.front();
			mQueue.pop();
			delete mStackPointer;
		}
		mStackPointer = nullptr;
		mMaxStackSize = other.mMaxStackSize;
		mSum = other.mSum;

		std::stack<T> tempStack;
		std::queue<std::stack<T>*> tempQueue = other.mQueue;
		for (size_t i = 0; i < other.mQueue.size(); i++)
		{
			tempStack = *tempQueue.front();
			mStackPointer = new std::stack<T>(tempStack);
			mQueue.push(mStackPointer);
			tempQueue.pop();
		}

		return *this;
	}


	template <typename T>
	void QueueStack<T>::Enqueue(const T& number)
	{
		if (GetCount() == 0)
		{
			mStackPointer = new std::stack<T>;
			mQueue.push(mStackPointer);
			mStackPointer->push(number);
			mSum += number;
			return;
		}
		if (mStackPointer->size() == mMaxStackSize)
		{
			mStackPointer = new std::stack<T>;
			mQueue.push(mStackPointer);
			mStackPointer->push(number);
			mSum += number;
			return;
		}
		mStackPointer->push(number);
		mSum += number;
		//큐 안의 스택이 복수일 때 Dequeue를 한다면 
		//다음에 들어갈 number 는 front의 stack에 들어가는가?
		//아니면 mStackPointer가 가리키고있는 stack에 들어가는가?
	}

	template <typename T>
	T QueueStack<T>::Peek() const
	{
		return mQueue.front()->top();
	}

	template <typename T>
	T QueueStack<T>::Dequeue()
	{
		if (mQueue.front()->size() == 1)
		{
			T temp1 = mQueue.front()->top();
			mSum -= temp1;
			std::stack<T>* tempStackPointer;
			tempStackPointer = mQueue.front();
			tempStackPointer->pop();
			mQueue.pop();
			delete tempStackPointer;
			return temp1;
		}
		T temp2 = mQueue.front()->top();
		mQueue.front()->pop();
		mSum -= temp2;
		return temp2;
	}

	template <typename T>
	T QueueStack<T>::GetMax()
	{
		T maxNumb = std::numeric_limits<T>().lowest();
		if (GetCount() == 0)
		{
			return maxNumb;
		}
		std::stack<T> tempStack;
		std::queue<std::stack<T>*> tempQueue = mQueue;
		unsigned int sizeHolder = 0;
		for (size_t i = 0; i < mQueue.size(); i++)
		{
			tempStack = *tempQueue.front();
			sizeHolder = tempStack.size();
			for (size_t j = 0; j < sizeHolder; j++)
			{
				if (maxNumb < tempStack.top())
				{
					maxNumb = tempStack.top();
				}
				tempStack.pop();
			}
			tempQueue.pop();
		}

		return maxNumb;
	}

	template <typename T>
	T QueueStack<T>::GetMin()
	{
		T minNumb = std::numeric_limits<T>().max();
		if (GetCount() == 0)
		{
			return minNumb;
		}
		std::stack<T> tempStack;
		std::queue<std::stack<T>*> tempQueue = mQueue;
		unsigned int sizeHolder = 0;
		for (size_t i = 0; i < mQueue.size(); i++)
		{
			tempStack = *tempQueue.front();
			sizeHolder = tempStack.size();
			for (size_t j = 0; j < sizeHolder; j++)
			{
				if (minNumb > tempStack.top())
				{
					minNumb = tempStack.top();
				}
				tempStack.pop();
			}
			tempQueue.pop();
		}

		return minNumb;
	}

	template <typename T>
	double QueueStack<T>::GetAverage()
	{
		double result = static_cast<double>(mSum) / static_cast<double>(GetCount());
		result = round(result * 1000.0) / 1000.0;
		return result;
	}

	template <typename T>
	T QueueStack<T>::GetSum()
	{
		/*
		T mSum = 0;
		std::stack<T> tempStack;
		std::queue<std::stack<T>*> tempQueue = mQueue;
		unsigned int sizeHolder = 0;
		for (size_t i = 0; i < mQueue.size(); i++)
		{
			tempStack = *tempQueue.front();
			sizeHolder = tempStack.size();
			for (size_t j = 0; j < sizeHolder; j++)
			{
				mSum += tempStack.top();
				tempStack.pop();
			}
			tempQueue.pop();
		}
		*/
		return mSum;
	}

	template <typename T>
	unsigned int QueueStack<T>::GetCount() const
	{
		
		if (mQueue.size() == 0)
		{
			return 0;
		}
		if (mQueue.size() == 1)
		{
			return mStackPointer->size();
		}
		unsigned int temp = mQueue.size() - 2;
		temp *= mMaxStackSize;
		temp += mStackPointer->size();
		temp += mQueue.front()->size();
		
		return temp;
	}

	template <typename T>
	unsigned int QueueStack<T>::GetStackCount() const
	{
		return mQueue.size();
	}

}