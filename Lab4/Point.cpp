#include "Point.h"

namespace lab4
{
	Point::Point()
		: mX(0.0f)
		, mY(0.0f)
	{

	}

	Point::Point(float x, float y)
		: mX(x)
		, mY(y)
	{

	}

	Point::~Point()
	{

	}

	Point::Point(const Point& other)
		: mX(other.mX)
		, mY(other.mY)
	{

	}


	Point Point::operator+(const Point& other) const
	{
		Point p(mX + other.mX, mY + other.mY);
		return p;
	}

	Point Point::operator-(const Point& other) const
	{
		Point p(mX - other.mX, mY - other.mY);
		return p;
	}

	float Point::Dot(const Point& other) const
	{
		float dotF = (mX * other.mX) + (mY * other.mY);
		return dotF;
	}

	Point Point::operator*(float operand) const
	{
		Point p(mX * operand, mY * operand);
		return p;
	}

	Point operator*(const float multiplier, const Point& other)
	{
		Point p = other * static_cast<float>(multiplier);
		return p;
	}

	float Point::GetX() const
	{
		return mX;
	}

	float Point::GetY() const
	{
		return mY;
	}

}