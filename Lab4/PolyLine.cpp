#include <cstring>
#include <cmath>
#include "PolyLine.h"

namespace lab4
{
	PolyLine::PolyLine()
		: mSize(0)
	{
		mPoints = new const Point * [10];

	}

	PolyLine::PolyLine(const PolyLine& other)
		: mSize(other.mSize)
	{
		mPoints = new const Point * [10];
		for (int i = 0; i < other.mSize; i++)
		{
			mPoints[i] = new Point(other.mPoints[i]->GetX(), other.mPoints[i]->GetY());
		}
	}

	PolyLine::~PolyLine()
	{
		for (int i = 0; i < mSize; i++)
		{
			delete mPoints[i];
		}
		delete[] mPoints;

	}

	bool PolyLine::AddPoint(float x, float y)
	{
		if (mSize == 10)
		{
			return false;
		}
		return AddPoint(new Point(x, y));
	}

	bool PolyLine::AddPoint(const Point* point)
	{
		if (mSize < 10)
		{
			mPoints[mSize] = point;
			mSize++;
			return true;
		}
		return false;
	}

	bool PolyLine::RemovePoint(unsigned int i)
	{
		if (static_cast<int>(i) < 0)
		{
			return false;
		}
		if (static_cast<int>(i) >= mSize)
		{
			return false;
		}
		delete mPoints[i];
		for (int j = i; j < mSize - 1; j++)
		{
			mPoints[j] = mPoints[j + 1];
		}

		mPoints[mSize - 1] = nullptr;
		mSize--;
		return true;
	}

	bool PolyLine::TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const
	{
		if (mSize == 0)
		{
			return false;
		}
		float minX, minY, maxX, maxY;
		minX = mPoints[0]->GetX();
		minY = mPoints[0]->GetY();
		maxX = mPoints[0]->GetX();
		maxY = mPoints[0]->GetY();

		for (int i = 1; i < mSize; i++)
		{
			if (minX > mPoints[i]->GetX())
			{
				minX = mPoints[i]->GetX();
			}
			if (minY > mPoints[i]->GetY())
			{
				minY = mPoints[i]->GetY();
			}
			if (maxX < mPoints[i]->GetX())
			{
				maxX = mPoints[i]->GetX();
			}
			if (maxY < mPoints[i]->GetY())
			{
				maxY = mPoints[i]->GetY();
			}
		}
		Point minP(minX, minY);
		Point maxP(maxX, maxY);

		*outMin = minP;
		*outMax = maxP;


		return true;
	}

	PolyLine& PolyLine::operator=(const PolyLine& other)
	{
		const Point** tmpPoints;
		tmpPoints = new const Point * [10];
		
		for (int i = 0; i < other.mSize; i++)
		{
			tmpPoints[i] = new Point(other.mPoints[i]->GetX(), other.mPoints[i]->GetY());
		}
		for (int i = 0; i < mSize; i++)
		{
			delete mPoints[i];
		}
		delete[] mPoints;
		
		mPoints = tmpPoints;

		mSize = other.mSize;


		return *this;
	}

	const Point* PolyLine::operator[](unsigned int i) const
	{
		if (static_cast<int>(i) < 0)
		{
			return nullptr;
		}
		if (static_cast<int>(i) >= mSize)
		{
			return nullptr;
		}
		return mPoints[static_cast<int>(i)];
	}

}