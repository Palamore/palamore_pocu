#include "Lab6.h"

#define SWAP(x, y) {int t; t = x; x = y; y = t;}

namespace lab6
{
	int Sum(const std::vector<int>& v)
	{
		int sum = 0;
		for (size_t i = 0; i < v.size(); i++)
		{
			sum += v.at(i);
		}
		return sum;
	}

	int Min(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return INT_MAX;
		}
		int min = v.at(0);
		for (size_t i = 1; i < v.size(); i++)
		{
			if (min > v.at(i))
			{
				min = v.at(i);
			}
		}
		return min;
	}

	int Max(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return INT_MIN;
		}
		int max = v.at(0);
		for (size_t i = 1; i < v.size(); i++)
		{
			if (max < v.at(i))
			{
				max = v.at(i);
			}
		}
		return max;
	}

	float Average(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0.0f;
		}
		float aver = 0.0f;
		aver = static_cast<float>(Sum(v)) / static_cast<float>(v.size());
		return aver;
	}

	int NumberWithMaxOccurrence(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}
		std::vector<int> uniqNumb;
		uniqNumb.reserve(v.size());
		std::vector<int> occurCount;
		occurCount.reserve(v.size());
		uniqNumb.push_back(v.at(0));
		occurCount.push_back(0);
		bool bIsIn = false;
		for (size_t i = 1; i < v.size(); i++)
		{
			for (size_t j = 0; j < uniqNumb.size(); j++)
			{
				if (uniqNumb.at(j) == v.at(i))
				{
					bIsIn = true;
					occurCount.at(j)++;
				}
			}
			if (!bIsIn)
			{
				uniqNumb.push_back(v.at(i));
				occurCount.push_back(0);
			}
			bIsIn = false;
		}


		int index = 0;
		int maxCount = 0;
		for (size_t i = 0; i < occurCount.size(); i++)
		{
			if (occurCount.at(i) > maxCount)
			{
				maxCount = occurCount.at(i);
				index = i;
			}
		}
		return uniqNumb.at(index);
	}

	void SortDescending(std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return;
		}
		for (size_t i = 0; i < v.size() - 1; i++)
		{
			for (size_t j = 0; j < v.size() - 1 - i; j++)
			{
				if (v.at(j) < v.at(j + 1))
				{
					SWAP(v.at(j), v.at(j + 1));
				}
			}
		}

	}
}