#include "Lab2.h"

namespace lab2
{   
	void PrintIntegers(std::istream& in, std::ostream& out)
	{
		int numb;
		int inputCount = 0;
		int* numbContainer = new int[2];
		int* tempContainer = new int[2];
		char trash;

		while (true) 
		{
			in >> numb;

			if (in.fail())
			{
				if (!in.eof())
				{
					in.clear();
					in >> trash;
					continue;
				}
				else
				{
					out << std::uppercase << std::right;
					out << std::setw(12) << "oct";
					out << std::setw(11) << "dec";
					out << std::setw(9) << "hex";
					out << "\n";
					out << "------------ ---------- --------\n";
					for (int i = 0; i < inputCount; i++)
					{
						out << std::setw(12) << std::oct << numbContainer[i];
						out << std::setw(11) << std::dec << numbContainer[i];
						out << std::setw(9) << std::hex << numbContainer[i];
						out << "\n";
					}
					break;
				}
			}
			else
			{
				delete[] tempContainer;
				inputCount++;
				tempContainer = new int[inputCount];
				for (int i = 0; i < inputCount - 1; i++)
				{
					tempContainer[i] = numbContainer[i];
				}
				tempContainer[inputCount - 1] = numb;
				delete[] numbContainer;
				numbContainer = new int[inputCount];
				for (int i = 0; i < inputCount; i++)
				{
					numbContainer[i] = tempContainer[i];
				}
			}
			if (in.eof())
			{
				out << std::uppercase << std::right;
				out << std::setw(12) << "oct";
				out << std::setw(11) << "dec";
				out << std::setw(9) << "hex";
				out << "\n";
				out << "------------ ---------- --------\n";
				for (int i = 0; i < inputCount; i++)
				{
					out << std::setw(12) << std::oct << numbContainer[i];
					out << std::setw(11) << std::dec << numbContainer[i];
					out << std::setw(9) << std::hex << numbContainer[i];
					out << "\n";
				}
				break;
			}
		}
		delete[] tempContainer;
		delete[] numbContainer;
	}

	void PrintMaxFloat(std::istream& in, std::ostream& out)
	{
		float numb;
		int inputCount = 0;
		float* numbContainer = new float[2];
		float* tempContainer = new float[2];
		char trash;

		float maxNumb = 0.0f;

		while (true)
		{
			in >> numb;

			if (in.fail())
			{
				if (!in.eof())
				{
					in.clear();
					in >> trash;
					continue;
				}
				else
				{
					out << std::fixed << std::setprecision(3) << std::right;
					for (int i = 0; i < inputCount; i++)
					{
						if (numbContainer[i] >= 0)
						{
							out << std::setw(6) << "+";
							out << std::setw(14) << numbContainer[i];
						}
						else
						{
							out << std::setw(6) << "-";
							out << std::setw(14) << numbContainer[i] * -1;
						}
						out << "\n";
					}
					if (maxNumb >= 0)
					{
						out << std::setw(5) << std::left << "max:" << std::right << "+";
						out << std::setw(14) << maxNumb;
					}
					else
					{
						out << std::setw(5) << std::left << "max:" << std::right << "-";
						out << std::setw(14) << maxNumb * -1;
					}
					out << "\n";
					break;
				}
			}
			else
			{
				if (inputCount == 0)
				{
					maxNumb = numb;
				}
				else
				{
					if (maxNumb < numb)
					{
						maxNumb = numb;
					}
				}
				inputCount++;
				delete[] tempContainer;
				tempContainer = new float[inputCount];
				for (int i = 0; i < inputCount - 1; i++)
				{
					tempContainer[i] = numbContainer[i];
				}
				tempContainer[inputCount - 1] = numb;
				delete[] numbContainer;
				numbContainer = new float[inputCount];
				for (int i = 0; i < inputCount; i++)
				{
					numbContainer[i] = tempContainer[i];
				}
			}
			if (in.eof())
			{
				out << std::fixed << std::setprecision(3) << std::right;
				for (int i = 0; i < inputCount; i++)
				{
					if (numbContainer[i] >= 0)
					{
						out << std::setw(6) << "+";
						out << std::setw(14) << numbContainer[i];
					}
					else
					{
						out << std::setw(6) << "-";
						out << std::setw(14) << numbContainer[i] * -1;
					}
					out << "\n";
				}
				if (maxNumb >= 0)
				{
					out << std::setw(5) << std::left << "max:" << std::right << "+";
					out << std::setw(14) << maxNumb;
				}
				else
				{
					out << std::setw(5) << std::left << "max:" << std::right << "-";
					out << std::setw(14) << maxNumb * -1;
				}
				out << "\n";
				break;
			}
			
		}
		delete[] numbContainer;
		delete[] tempContainer;
	}
}