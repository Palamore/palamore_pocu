#pragma once
#include <stdint.h>

namespace lab8
{
	template <size_t N>
	class FixedVector<bool, N>
	{
	public:
		FixedVector();
		~FixedVector();


		FixedVector(const FixedVector& other);
		FixedVector& operator=(const FixedVector& other);

		bool Add(const bool& b);
		bool Remove(const bool& b);
		const bool Get(unsigned int index) const;
		const bool operator[](unsigned int index) const;
		int GetIndex(const bool& b) const;
		size_t GetSize() const;
		size_t GetCapacity() const;

	private:
		int16_t mSize;
		int16_t mCapacity;
		int32_t mVector[N / 31 + 1];

	};

	template <size_t N>
	FixedVector<bool, N>::FixedVector()
		: mSize(0)
		, mCapacity(N)
	{
		for (size_t i = 0; i < N / 31 + 1; i++)
		{
			mVector[i] = 0;
		}
	}

	template <size_t N>
	FixedVector<bool, N>::~FixedVector()
	{

	}

	template <size_t N>
	FixedVector<bool, N>::FixedVector(const FixedVector& other)
		: mSize(other.mSize)
		, mCapacity(other.mCapacity)
	{
		for (size_t i = 0; i < static_cast<size_t>(other.mCapacity / 31 + 1); i++)
		{
			mVector[i] = other.mVector[i];
		}
	}

	template <size_t N>
	FixedVector<bool, N>& FixedVector<bool, N>::operator=(const FixedVector& other)
	{
		mSize = other.mSize;
		mCapacity = other.mCapacity;
		for (size_t i = 0; i < static_cast<size_t>(other.mCapacity / 31 + 1); i++)
		{
			mVector[i] = other.mVector[i];
		}
		return *this;
	}

	template <size_t N>
	bool FixedVector<bool, N>::Add(const bool& b)
	{
		if (mSize == mCapacity)
		{
			return false;
		}
		if (b)
		{
			mVector[mSize / 31] += static_cast<int32_t>(pow(2, mSize % 31));
		}
		mSize++;
		return true;
	}

	template <size_t N>
	bool FixedVector<bool, N>::Remove(const bool& b)
	{
		int32_t temp = 0;
		for (size_t i = 0; i < static_cast<size_t>(mSize); i++)
		{
			if (b)
			{
				if (mVector[i / 31] & static_cast<int32_t>(pow(2, i % 31)))
				{
					temp = mVector[i / 31] % static_cast<int32_t>(pow(2, i % 31));
					mVector[i / 31] = mVector[i / 31] >> ((i % 31) + 1);
					mVector[i / 31] = mVector[i / 31] << (i % 31);
					mVector[i / 31] += temp;
					for (size_t j = i / 31; j < sizeof(mVector) / sizeof(int32_t) - 1; j++)
					{
						if (mVector[j + 1] & static_cast<int32_t>(pow(2, 0)))
						{
							mVector[j] += static_cast<int32_t>(pow(2, 30));
						}
						mVector[j + 1] = mVector[j + 1] >> 1;
					}
					mSize--;
					return true;
				}
			}
			else
			{
				if (!(mVector[i / 31] & static_cast<int32_t>(pow(2, i % 31))))
				{
					temp = mVector[i / 31] % static_cast<int32_t>(pow(2, i % 31));
					mVector[i / 31] = mVector[i / 31] >> ((i % 31) + 1);
					mVector[i / 31] = mVector[i / 31] << (i % 31);
					mVector[i / 31] += temp;
					for (size_t j = i / 31; j < sizeof(mVector) / sizeof(int32_t) - 1; j++)
					{
						if (mVector[j + 1] & static_cast<int32_t>(pow(2, 0)))
						{
							mVector[j] += static_cast<int32_t>(pow(2, 30));
						}
						mVector[j + 1] = mVector[j + 1] >> 1;
					}
					mSize--;
					return true;
				}
			}
		}
		return false;
	}

	template <size_t N>
	const bool FixedVector<bool, N>::Get(unsigned int index) const
	{
		if (index >= static_cast<unsigned int>(mSize))
		{
			return false;
		}
		if (mVector[index / 31] & static_cast<int32_t>(pow(2, index % 31)))
		{
			return true;
		}
		return false;
	}

	template <size_t N>
	const bool FixedVector<bool, N>::operator[](unsigned int index) const
	{
		if (mVector[index / 31] & static_cast<int32_t>(pow(2, index % 31)))
		{
			return true;
		}
		return false;
	}

	template <size_t N>
	int FixedVector<bool, N>::GetIndex(const bool& b) const
	{
		for (size_t i = 0; i < static_cast<size_t>(mSize); i++)
		{
			if (b) 
			{
				if (mVector[i / 31] & static_cast<int32_t>(pow(2, i % 31)))
				{
					return i;
				}
			}
			else
			{
				if (!(mVector[i / 31] & static_cast<int32_t>(pow(2, i % 31))))
				{
					return i;
				}
			}
		}
		return -1;
	}

	template <size_t N>
	size_t FixedVector<bool, N>::GetSize() const
	{
		return static_cast<size_t>(mSize);
	}

	template <size_t N>
	size_t FixedVector<bool, N>::GetCapacity() const
	{
		return static_cast<size_t>(mCapacity);
	}


}