#pragma once



namespace lab8
{
	template <typename T, size_t N>
	class FixedVector
	{
	public:
		FixedVector();
		virtual ~FixedVector();

		FixedVector(const FixedVector& other);
		FixedVector& operator=(const FixedVector& other);

		bool Add(const T& t);
		bool Remove(const T& t);
		const T& Get(unsigned int index) const;
		T& operator[](unsigned int index) const;
		int GetIndex(const T& t) const;
		size_t GetSize() const;
		size_t GetCapacity() const;



	private:
		T* mVector;
		size_t mSize;
		size_t mCapacity;

	};

	template <typename T, size_t N>
	FixedVector<T, N>::FixedVector()
		: mSize(0)
		, mCapacity(N)
	{
		mVector = new T[N];
	}

	template <typename T, size_t N>
	FixedVector<T, N>::~FixedVector()
	{
		delete[] mVector;
	}

	template <typename T, size_t N>
	FixedVector<T, N>::FixedVector(const FixedVector& other)
		: mSize(other.mSize)
		, mCapacity(other.mCapacity)
	{
		mVector = new T[other.mCapacity];
		for (size_t i = 0; i < other.mSize; i++)
		{
			mVector[i] = other.Get(i);
		}
	}

	template <typename T, size_t N>
	FixedVector<T, N>& FixedVector<T, N>::operator=(const FixedVector& other)
	{
		if (mVector == other.mVector)
		{
			return *this;
		}
		mSize = other.mSize;
		mCapacity = other.mCapacity;
		delete[] mVector;

		mVector = new T[other.mCapacity];
		for (size_t i = 0; i < other.mSize; i++)
		{
			mVector[i] = other.Get(i);
		}
		return *this;
	}

	template <typename T, size_t N>
	bool FixedVector<T, N>::Add(const T& t)
	{
		if (mSize == mCapacity)
		{
			return false;
		}
		mVector[mSize] = t;
		mSize++;
		return true;
	}

	template <typename T, size_t N>
	bool FixedVector<T, N>::Remove(const T& t)
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mVector[i] == t)
			{
				for (size_t j = i; j < mSize - 1; j++)
				{
					mVector[j] = mVector[j + 1];
				}
				mVector[mSize - 1] = 0;
				mSize--;
				return true;
			}
		}
		return false;
	}

	template <typename T, size_t N>
	const T& FixedVector<T, N>::Get(unsigned int index) const
	{
		return mVector[index];
	}

	template <typename T, size_t N>
	T& FixedVector<T, N>::operator[](unsigned int index) const
	{
		return mVector[index];
	}

	template <typename T, size_t N>
	int FixedVector<T, N>::GetIndex(const T& t) const
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mVector[i] == t)
			{
				return static_cast<int>(i);
			}
		}
		return -1;
	}

	template <typename T, size_t N>
	size_t FixedVector<T, N>::GetSize() const
	{
		return mSize;
	}

	template <typename T, size_t N>
	size_t FixedVector<T, N>::GetCapacity() const
	{
		return mCapacity;
	}


}