#include <iostream>

#include "TimeSheet.h"

int main()
{
	lab3::TimeSheet p("Pope", 6);
	p.AddTime(3);
	p.AddTime(4);
	p.AddTime(5);
	p.AddTime(6);
	p.AddTime(6);
	p.AddTime(6);
	std::cout << p.GetAverageTime();


	return 0;
}