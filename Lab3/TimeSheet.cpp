#include "TimeSheet.h"

namespace lab3
{
	TimeSheet::TimeSheet(const char* name, unsigned int maxEntries)
		: mName(name)
		, mMaxEntries(maxEntries)
		, mSize(0)
	{
		mTimeContainer = new int[maxEntries];

	}

	TimeSheet::TimeSheet(const TimeSheet& ts)
		: mName(ts.mName)
		, mMaxEntries(ts.mMaxEntries)
		, mSize(ts.mSize)
	{
		mTimeContainer = new int[ts.mMaxEntries];
		for (int i = 0; i < ts.mSize; i++)
		{
			mTimeContainer[i] = ts.mTimeContainer[i];
		}
	}
	
	TimeSheet& TimeSheet::operator=(const TimeSheet& rhs)
	{
		mName = rhs.mName;
		mMaxEntries = rhs.mMaxEntries;
		mSize = rhs.mSize;

		int* tempContainer = new int[rhs.mMaxEntries];
		for (int i = 0; i < rhs.mSize; i++)
		{
			tempContainer[i] = rhs.mTimeContainer[i];
		}

		delete[] mTimeContainer;
		mTimeContainer = new int[rhs.mMaxEntries];
		for (int i = 0; i < rhs.mSize; i++)
		{
			mTimeContainer[i] = tempContainer[i];
		}
		delete[] tempContainer;
		
		return *this;

	}

	TimeSheet::~TimeSheet()
	{
		delete[] mTimeContainer;
	}

	void TimeSheet::AddTime(int timeInHours)
	{
		if (timeInHours <= 0 || timeInHours >= 11)
		{
			return;
		}
		if (mMaxEntries == mSize)
		{
			return;
		}

		mSize++;
		mTimeContainer[mSize - 1] = timeInHours;
	}

	int TimeSheet::GetTimeEntry(unsigned int index) const
	{
		if (static_cast<unsigned int>(mSize) - 1 >= index)
		{
			return mTimeContainer[index];
		}
		else
		{
			return -1;
		}
	}

	int TimeSheet::GetTotalTime() const
	{
		int sum = 0;
		for (int i = 0; i < mSize; i++)
		{
			sum += mTimeContainer[i];
		}
		return sum;
	}

	float TimeSheet::GetAverageTime() const
	{
		if (mSize == 0)
		{
			return 0.0f;
		}
		float result = 0.0f;
		
		result = static_cast<float>(GetTotalTime()) / static_cast<float>(mSize);
		return result;
	}

	float TimeSheet::GetStandardDeviation() const
	{
		if (mSize == 0)
		{
			return 0.0f;
		}
		float sum = 0.0f;
		float aver = GetAverageTime();
		float temp = 0.0f;
		for (int i = 0; i < mSize; i++)
		{
			temp = static_cast<float>(mTimeContainer[i]) - aver;

			sum += (temp * temp);
		}
		return static_cast<float>(sqrt(sum / static_cast<float>(mSize)));
	}

	const std::string& TimeSheet::GetName() const
	{
		return mName;
	}

}