#pragma once

#include <memory>

namespace lab11
{
	template <typename T>
	class Storage
	{
	public:
		Storage(unsigned int length);
		Storage(unsigned int length, const T& initialValue);
		Storage(Storage<T>&& other);
		Storage(Storage<T>& other);

		Storage& operator=(Storage<T>&& other);
		Storage& operator=(Storage<T>& other);

		bool Update(unsigned int index, const T& data);
		const std::unique_ptr<T[]>& GetData() const;
		unsigned int GetSize() const;
		
	private:
		std::unique_ptr<T[]> mStorage;
		unsigned int mSize;
	};

	template <typename T>
	Storage<T>::Storage(unsigned int length)
		: mSize(length)
		, mStorage(std::make_unique<T[]>(length))
	{
		memset(&mStorage[0], 0, length * sizeof(T));
	}

	template <typename T>
	Storage<T>::Storage(unsigned int length, const T& initialValue)
		: mSize(length)
		, mStorage(std::make_unique<T[]>(length))
	{
		for (size_t i = 0; i < length; i++)
		{
			mStorage[i] = initialValue;
		}
	}

	template <typename T>
	Storage<T>::Storage(Storage<T>&& other)
		: mStorage(std::move(other.mStorage))
		, mSize(other.mSize)
	{
		other.mStorage = nullptr;
		other.mSize = 0;
	}

	template <typename T>
	Storage<T>::Storage(Storage<T>& other)
		: mStorage(std::make_unique<T[]>(other.mSize))
		, mSize(other.mSize)
	{
		for (size_t i = 0; i < other.mSize; i++)
		{
			mStorage[i] = other.mStorage[i];
		}
	}

	template <typename T>
	Storage<T>& Storage<T>::operator=(Storage<T>&& other)
	{
		if (this->mStorage.get() == other.mStorage.get())
		{
			return *this;
		}
		mStorage.reset();

		mStorage = std::move(other.mStorage);
		mSize = other.mSize;

		other.mStorage = nullptr;
		other.mSize = 0;

		return *this;
	}

	template <typename T>
	Storage<T>& Storage<T>::operator=(Storage<T>& other)
	{
		if (this->mStorage.get() == other.mStorage.get())
		{
			return *this;
		}
		mStorage.reset();
		mStorage = std::make_unique<T[]>(other.mSize);

		for (size_t i = 0; i < other.mSize; i++)
		{
			mStorage[i] = other.mStorage[i];
		}
		mSize = other.mSize;

		return *this;
	}

	template <typename T>
	bool Storage<T>::Update(unsigned int index, const T& data)
	{
		if (index >= mSize)
		{
			return false;
		}
		mStorage[index] = data;
		return true;
	}

	template <typename T>
	const std::unique_ptr<T[]>& Storage<T>::GetData() const
	{
		return mStorage;
	}

	template <typename T>
	unsigned int Storage<T>::GetSize() const
	{
		return mSize;
	}


}