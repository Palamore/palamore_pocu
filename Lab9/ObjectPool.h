#pragma once
#include <queue>

namespace lab9
{
	template <typename T>
	class ObjectPool final
	{
	public:
		ObjectPool(size_t maxPoolSize);
		~ObjectPool();
		ObjectPool(const ObjectPool& other) = delete;

		ObjectPool& operator=(const ObjectPool& other) = delete;
		T* Get();
		void Return(T* t);
		size_t GetFreeObjectCount();
		size_t GetMaxFreeObjectCount();

	private:
		std::queue<T*> mObjectPool;
		size_t mMaxPoolSize;
	};

	template <typename T>
	ObjectPool<T>::ObjectPool(size_t maxPoolSize)
		: mMaxPoolSize(maxPoolSize)
	{
		
	}

	template <typename T>
	ObjectPool<T>::~ObjectPool()
	{
		size_t poolSize = mObjectPool.size();
		T* temp;
		for (size_t i = 0; i < poolSize; i++)
		{
			temp = mObjectPool.front();
			delete temp;
			mObjectPool.pop();
		}
	}

	template <typename T>
	T* ObjectPool<T>::Get()
	{
		if (mObjectPool.size() == 0)
		{
			T* newOne = new T;
			return newOne;
		}

		T* object = mObjectPool.front();
		mObjectPool.pop();
		return object;
	}

	template <typename T>
	void ObjectPool<T>::Return(T* t)
	{
		if (mObjectPool.size() == mMaxPoolSize)
		{
			delete t;
			return;
		}
		mObjectPool.emplace(t);
	}

	template <typename T>
	size_t ObjectPool<T>::GetFreeObjectCount()
	{
		return mObjectPool.size();
	}

	template <typename T>
	size_t ObjectPool<T>::GetMaxFreeObjectCount()
	{
		return mMaxPoolSize;
	}

}